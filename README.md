# For DEV

The mobile application was created with EXPO-CLI with the typescript template.

The entry for the mobile app is `./App.tsx`.

## Before running

Make sure you have all the modules installed and up to date

run: `npm install`

## To start locally

Run either of the below command

`npm run start` **or** `expo start`

Will launch up a dashboard in you browser. From here you can test as you please.

- You can attach it to an emulator
- You can install the expo app on your phone and simply scan the QR code from the dashboard
- You can run it in a browser tab ( **IMPORTANT**: this will not work correctly since not all native components have proper web counterparts)

#Expo related

> Why do I have a folder named ".expo-shared" in my project?

The ".expo-shared" folder is created when running commands that produce state that is intended to be shared with all developers on the project. For example, "npx expo-optimize".

> What does the "assets.json" file contain?

The "assets.json" file describes the assets that have been optimized through "expo-optimize" and do not need to be processed again.

> Should I commit the ".expo-shared" folder?

Yes, you should share the ".expo-shared" folder with your collaborators.
