import React from 'react';
import { StatusBar as _StatusBar } from 'react-native';
import { useSelector } from 'react-redux';
import themeSelector from '../../../constants/themeSelector';

const StatusBar = () => {
  const theme = useSelector(themeSelector.theme);

  return <_StatusBar hidden={false} backgroundColor={theme.headerBackground} barStyle={'light-content'} />;
};

export default StatusBar;
