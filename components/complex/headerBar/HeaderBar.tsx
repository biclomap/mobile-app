import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import themeSelector from '../../../constants/themeSelector';
import { calcUnderlayColor } from '../../../constants/Utils';

import { BackButton, Container, Title } from './styled';

interface IProps {
  title: string;
  back?: string;
}

const HeaderBar = (props: IProps) => {
  const { title, back } = props;

  const theme = useSelector(themeSelector.theme);
  const navigator = useNavigation();

  const handlePressBack = () => back && navigator.navigate(back);

  const underlayColor = calcUnderlayColor(theme.main);

  return (
    <Container theme={theme}>
      {back && (
        <BackButton underlayColor={underlayColor} onPress={handlePressBack}>
          <FontAwesomeIcon icon={faArrowLeft} size={24} color={theme.headerText} />
        </BackButton>
      )}
      <Title theme={theme}>{title}</Title>
    </Container>
  );
};

export default HeaderBar;
