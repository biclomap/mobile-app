import styled from 'styled-components/native';

import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';

export const Container = styled.View<{ theme: ITheme }>`
  background-color: ${(props) => props.theme.headerBackground || 'white'};
  height: 56px;
  width: ${Layout.window.width}px;
  align-items: center;
  justify-content: center;
  margin-left: -16px;
`;

export const Title = styled.Text<{ theme: ITheme }>`
  color: ${(props) => props.theme.headerText || 'black'};
  font-size: ${Layout.font.sizeFactor * 3}px;
`;

export const BackButton = styled.TouchableHighlight`
  position: absolute;
  height: 100%;
  padding: 0 ${Layout.layout.gap}px;
  justify-content: center;
  align-self: baseline;
`;
