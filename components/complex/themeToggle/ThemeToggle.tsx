import React from 'react';
import { View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { CHANGE_THEME } from '../../../constants/themeConstants';
import themeSelector from '../../../constants/themeSelector';

import Text from '../../basic/text/Text';
import Title from '../../basic/text/Title';
import Toggle from '../../basic/toggle/Toggle';

import { OneLine } from './styled';

const ThemeToggle = () => {
  const theme = useSelector(themeSelector.theme);

  const dispatch = useDispatch();

  const handleChange = (value: boolean) => {
    dispatch({ type: CHANGE_THEME, data: value ? 'dark' : 'light' });
  };

  return (
    <View>
      <Title>You can change the theme</Title>
      <OneLine>
        <Text>Toggle theme:</Text>
        <Toggle value={theme.name === 'Dark'} onChange={handleChange} />
      </OneLine>
      <Text>{theme.name}</Text>
    </View>
  );
};

export default ThemeToggle;
