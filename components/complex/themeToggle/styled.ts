import styled from 'styled-components/native';

export const OneLine = styled.View`
  flex-direction: row;
  align-items: center;
`;
