import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Animated, Insets, LayoutChangeEvent, Modal } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faCog, faPlayCircle } from '@fortawesome/free-solid-svg-icons';

import themeSelector from '../../../constants/themeSelector';
import Layout from '../../../constants/Layout';

import LabeledDropdown from '../../basic/dropdown/LabeledDropdown';
import { IOption } from '../../basic/dropdown/Dropdown';
import Title from '../../basic/text/Title';
import Text from '../../basic/text/Text';
import { calcUnderlayColor } from '../../../constants/Utils';

import { Button, Bottom, MainContainer, ModalBody } from './styled';

interface IProps {
  visible: boolean;
  onClose: () => void;
}

const options: IOption[] = [
  { label: <Text>Can't select this</Text>, value: 1 },
  { label: <Text>Can't select this either</Text>, value: 2 },
  { label: <Text>Can't select this either</Text>, value: 3 },
  { label: <Text>Can't select this either</Text>, value: 4 },
  { label: <Text>Can't select this either</Text>, value: 5 },
  { label: <Text>Can't select this either</Text>, value: 6 },
  { label: <Text>Can't select this either</Text>, value: 7 },
  { label: <Text>Can't select this either</Text>, value: 8 },
  { label: <Text>Can't select this either</Text>, value: 9 },
];

const NewRideModal = (props: IProps) => {
  const { visible, onClose } = props;

  const [offset, setOffset] = useState(0);

  const translated = new Animated.Value(offset);
  const animationDuration = 300;

  const theme = useSelector(themeSelector.theme);

  const animateSlide = (direction: 'up' | 'down') => {
    Animated.timing(translated, {
      toValue: direction === 'up' ? 0 : offset,
      duration: animationDuration,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    animateSlide(visible ? 'up' : 'down');
  }, [visible, offset]);

  const getLayoutInfo = (event: LayoutChangeEvent) => setOffset(Math.ceil(event.nativeEvent.layout.height));

  const underlayColor = calcUnderlayColor(theme.main);

  const slop = Math.ceil(Layout.layout.gap / 2);
  const hitSlop: Insets = {
    top: slop,
    bottom: slop,
    left: slop,
    right: slop,
  };

  const handleClose = () => {
    animateSlide('down');
    setTimeout(onClose, animationDuration - Math.ceil(animationDuration / 15));
  };

  return (
    <Modal transparent={true} animationType={'fade'} visible={visible}>
      <MainContainer>
        <Animated.View style={{ width: '100%', transform: [{ translateY: translated }] }}>
          <ModalBody theme={theme} onLayout={getLayoutInfo}>
            <Title>Record a new ride</Title>
            <LabeledDropdown label={'Route'} value={0} options={options} callToActionText={'No route selected'} />
            <LabeledDropdown label={'Bike'} value={0} options={options} callToActionText={'No bike selected'} />
            <Bottom>
              <Button underlayColor={underlayColor} onPress={handleClose} hitSlop={hitSlop}>
                <FontAwesomeIcon icon={faArrowLeft} size={24} color={theme.headerText} />
              </Button>
              <Button underlayColor={underlayColor}>
                <FontAwesomeIcon icon={faPlayCircle} size={56} color={theme.main} />
              </Button>
              <Button underlayColor={underlayColor}>
                <FontAwesomeIcon icon={faCog} size={24} color={theme.headerText} />
              </Button>
            </Bottom>
          </ModalBody>
        </Animated.View>
      </MainContainer>
    </Modal>
  );
};

export default NewRideModal;
