import styled from 'styled-components/native';
import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';

export const MainContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: flex-end;
  background-color: rgba(123, 123, 123, 0.5);
`;

export const ModalBody = styled.View<{ theme: ITheme }>`
  width: 100%;
  padding: ${Layout.layout.gap}px;
  background-color: ${(props) => props.theme.card};
  align-items: center;
`;

export const Bottom = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: ${Layout.layout.gap}px;
`;

export const Button = styled.TouchableHighlight`
  justify-content: center;
`;
