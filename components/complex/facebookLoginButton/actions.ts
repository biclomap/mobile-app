import { Action, ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IStore } from '../../../store';
import { IAction } from '../../screens/login/reducer';

export const getFacebookUserDetails: ActionCreator<ThunkAction<Promise<Action>, IStore, void, any>> = (token: string) => {
  return async (dispatch: Dispatch<IAction>): Promise<any> => {
    try {
      const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
      const userDetails = await response.json();
      console.log(JSON.stringify(userDetails, null, 2));
      return dispatch({
        type: 'authentication/login',
        data: {
          type: 'facebook',
          token: token,
          name: userDetails.name,
        },
      });
    } catch (e) {}
  };
};
