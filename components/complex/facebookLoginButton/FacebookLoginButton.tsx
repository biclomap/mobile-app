import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import * as FaceBook from 'expo-facebook';

import appSettings from '../../../app.json';

import Button from '../../basic/button/Button';
import { getFacebookUserDetails } from './actions';

interface IProps {
  onSuccess: () => void;
}

const FacebookLoginButton = (props: IProps) => {
  const { onSuccess } = props;
  const [buffer, setBuffer] = useState(false);

  const dispatch = useDispatch();

  const handleFacebookLoginPress = async () => {
    setBuffer(true);
    try {
      await FaceBook.initializeAsync({ appId: appSettings.expo.facebookAppId });
      const user = await FaceBook.logInWithReadPermissionsAsync({
        permissions: ['public_profile', 'email', 'user_photos'],
      });
      if (user.type === 'success') {
        const token = user.token;
        await dispatch(getFacebookUserDetails(token));
        onSuccess && onSuccess();
      }
      setBuffer(false);
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
      setBuffer(false);
    }
  };

  return (
    <Button type={'main'} title={'Continue with Facebook'} onPress={handleFacebookLoginPress} isLoading={buffer} />
  );
};

export default FacebookLoginButton;
