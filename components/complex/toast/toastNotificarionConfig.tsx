import React from 'react';
import { INotificationTypes, IToastProps } from './ToastNotification';
import Notification from './Notification';
import { faCheckCircle, faDotCircle, faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';

const toastNotificationConfig: { [key in INotificationTypes]: ({ text1, props }: IToastProps) => JSX.Element } = {
  success: ({ text1, props }: IToastProps) => (
    <Notification
      text={text1}
      props={{ icon: props.icon || faCheckCircle, onPress: props.onPress }}
      color={'success'}
    />
  ),
  info: ({ text1, props }: IToastProps) => (
    <Notification text={text1} props={{ icon: props.icon || faDotCircle, onPress: props.onPress }} color={'main'} />
  ),
  warning: ({ text1, props }: IToastProps) => (
    <Notification
      text={text1}
      props={{ icon: props.icon || faQuestionCircle, onPress: props.onPress }}
      color={'warning'}
    />
  ),
  danger: ({ text1, props }: IToastProps) => (
    <Notification text={text1} props={{ icon: props.icon || faExclamation, onPress: props.onPress }} color={'danger'} />
  ),
};

export default toastNotificationConfig;
