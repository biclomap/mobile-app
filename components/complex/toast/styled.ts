import styled from 'styled-components/native';
import Layout from '../../../constants/Layout';

export const Container = styled.TouchableHighlight<{ color: string }>`
  background-color: ${(props) => props.color || 'white'};
  width: ${Math.ceil(Layout.window.width - 2 * Layout.layout.gap)}px;
  padding: ${Math.ceil(Layout.layout.gap / 3)}px;
  border-radius: ${Layout.layout.roundness}px;
`;

export const FlexBox = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const MainPart = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Icon = styled.View`
  margin-right: ${Math.ceil(Layout.layout.gap / 6)}px;
`;

export const CloseIcon = styled.View``;
