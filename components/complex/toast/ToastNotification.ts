import Toast from 'react-native-toast-message';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons/';

interface IProps {
  text?: string;
  icon?: IconDefinition;
}

export type INotificationTypes = 'success' | 'info' | 'warning' | 'danger';

interface IShowNotification extends IProps {
  type: INotificationTypes;
}

export interface IToastProps {
  text1: string;
  props: { icon?: IconDefinition; onPress?: () => void };
}

const showNotification = (props: IShowNotification) => {
  Toast.show({ type: props.type, text1: props.text, props: { icon: props.icon, onPress: () => Toast.hide() } });
};

const ToastNotification = {
  success: (props: IProps) => showNotification({ ...props, type: 'success' }),
  info: (props: IProps) => showNotification({ ...props, type: 'info' }),
  warning: (props: IProps) => showNotification({ ...props, type: 'warning' }),
  danger: (props: IProps) => showNotification({ ...props, type: 'danger' }),
};

export default ToastNotification;
