import React from 'react';
import { useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faTimes, IconDefinition } from '@fortawesome/free-solid-svg-icons';

import themeSelector from '../../../constants/themeSelector';
import { IColors } from '../../../constants/Colors';
import Text from '../../basic/text/Text';

import { CloseIcon, Container, FlexBox, Icon, MainPart } from './styled';

interface IProps {
  text: string;
  props: { icon: IconDefinition; onPress?: () => void };
  color: IColors;
}

const Notification = ({ text, props, color }: IProps) => {
  const theme = useSelector(themeSelector.theme);

  return (
    <Container color={theme[color]} onPress={() => props.onPress && props.onPress()} underlayColor={theme[color]}>
      <FlexBox>
        <MainPart>
          {props.icon && (
            <Icon>
              <FontAwesomeIcon icon={props.icon} color={theme.card} />
            </Icon>
          )}
          <Text color={theme.card}>{text}</Text>
        </MainPart>
        <CloseIcon>
          <FontAwesomeIcon icon={faTimes} color={theme.card} />
        </CloseIcon>
      </FlexBox>
    </Container>
  );
};

export default Notification;
