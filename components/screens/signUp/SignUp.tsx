import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import { AuthNavigationNames } from '../../../navigation/LinkingConfiguration';
import { authRequestStatusSelector, cleanupRequestStatus, signUpAction } from '../../../redux/Authentication';

import Text from '../../basic/text/Text';
import Divider from '../../basic/text/Divider';
import { Screen } from '../../basic/screen/Screen';
import Layout from '../../../constants/Layout';
import LabeledInput from '../../basic/input/LabeledInput';
import Button from '../../basic/button/Button';

import { Form } from './styled';

const SignUp = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const authRequest = useSelector(authRequestStatusSelector);

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const dividerWidth = Math.ceil(Layout.window.width / 2);

  const onPressSignUp = () => dispatch(signUpAction({ email, password }));

  useEffect(() => {
    if (authRequest.success.includes(signUpAction.typePrefix)) {
      navigation.navigate(AuthNavigationNames.Login);
    }
    return () => {
      dispatch(cleanupRequestStatus(signUpAction.typePrefix));
    };
  }, [dispatch, navigation]);

  return (
    <Screen>
      <Form justify={'center'}>
        <LabeledInput label={'E-mail'} placeholder={'your e-mail'} value={email} onChange={setEmail} />
        <LabeledInput label={'Password'} placeholder={'your password of choice'} value={password} onChange={setPassword} secure={true} />
        <Button type={'main'} title={'Sign Up'} onPress={onPressSignUp} isLoading={authRequest.pending.includes(signUpAction.typePrefix)} />
        <Divider width={dividerWidth} />
        <Text justify={'center'}>After Signing up will receive an e-mail with a verification link.</Text>
        <Text justify={'center'}>After accessing the provided link you will be able to log in with the above provided credentials.</Text>
      </Form>
    </Screen>
  );
};

export default SignUp;
