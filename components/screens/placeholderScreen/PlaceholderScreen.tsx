import React from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import { useRoute } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import themeSelector from '../../../constants/themeSelector';
import Layout from '../../../constants/Layout';

import Card from '../../basic/card/Card';
import Title from '../../basic/text/Title';
import Description from '../../basic/text/Description';
import Divider from '../../basic/text/Divider';

import { CenterPositioner } from './styled';
import { faProcedures } from '@fortawesome/free-solid-svg-icons';

const PlaceholderScreen = () => {
  const { name } = useRoute();
  const theme = useSelector(themeSelector.theme);

  return (
    <View>
      <CenterPositioner>
        <Card justify={'center'}>
          <Title>{name}</Title>
          <Divider width={150} />
          <FontAwesomeIcon icon={faProcedures} color={theme.mainText} size={50} />
          <View style={{ height: Layout.layout.gap }} />
          <Description>This page is not implemented.</Description>
        </Card>
      </CenterPositioner>
    </View>
  );
};

export default PlaceholderScreen;
