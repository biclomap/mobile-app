import React from 'react';
import { useNavigation, useNavigationState } from '@react-navigation/native';

import FacebookLoginButton from '../../complex/facebookLoginButton/FacebookLoginButton';
import Layout from '../../../constants/Layout';
import Button from '../../basic/button/Button';
import Divider from '../../basic/text/Divider';
import Text from '../../basic/text/Text';
import Title from '../../basic/text/Title';

import { ButtonsGroup, Container, Form, JustifyCenter, Logo, Lower, Upper } from './styled';
import { AppNavigationNames, AuthNavigationNames } from '../../../navigation/LinkingConfiguration';
import { useBackHandler } from '@react-native-community/hooks';

const AuthLanding = () => {
  const navigation = useNavigation();

  const width = Layout.window.width - Layout.layout.gap * 6;
  const logoSize = Math.ceil(Math.min(Layout.window.width / 2, Layout.window.height / 6));

  const sendToLogin = () => navigation.navigate(AuthNavigationNames.Login);

  const sendToSignUp = () => navigation.navigate(AuthNavigationNames.SignUp);
  
  const onFacebookLogin = () => {
    navigation.navigate(AppNavigationNames.Root);
  };

  const route = useNavigationState((state) => state.routes[state.index]);

  useBackHandler(() => {
    if (route.name === AuthNavigationNames.Landing) {
      return true;
    }
    return false;
  });

  return (
    <Container>
      <Upper offset={Math.ceil(logoSize / 3)}>
        <Logo source={require('../../../assets/images/icon.png')} size={logoSize} />
        <Text>This logo is temporary</Text>
      </Upper>
      <Lower>
        <Form width={'100%'} justify={'center'}>
          <Title justify={'center'}>Sign up</Title>
          <Text size={1} justify={'center'}>
            * These are not yet functional
          </Text>
          <ButtonsGroup>
            <FacebookLoginButton onSuccess={onFacebookLogin} />
            <Button title={'Continue with Google'} />
            <Button title={'Sign up using Email'} />
          </ButtonsGroup>
          <JustifyCenter>
            <Divider width={width} />
          </JustifyCenter>
          <Text justify={'center'}>Log in here for now</Text>
          <Button title={'Login'} type={'main'} onPress={sendToLogin} />
          <Button title={'Sign Up'} type={'default'} onPress={sendToSignUp} />
        </Form>
      </Lower>
    </Container>
  );
};

export default AuthLanding;
