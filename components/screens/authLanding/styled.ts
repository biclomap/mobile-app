import styled from 'styled-components/native';
import Layout from '../../../constants/Layout';
import Card from '../../basic/card/Card';

export const Container = styled.View`
  flex: 1;
  justify-content: space-between;
  padding: ${Layout.layout.gap}px;
`;

export const Upper = styled.View<{ offset?: number }>`
  align-items: center;
  margin-top: ${(props) => props.offset || Layout.layout.gap}px;
`;

export const Lower = styled.View`
  align-items: center;
  width: 100%;
`;

export const Logo = styled.Image<{ size?: number }>`
  height: ${(props) => props.size || Layout.layout.gap}px;
  width: ${(props) => props.size || Layout.layout.gap}px;
`;

export const Form = styled(Card)`
  justify-content: center;
`;

export const JustifyCenter = styled.View`
  width: 100%;
  align-items: center;
`;

export const ButtonsGroup = styled.View`
  margin-top: ${Layout.layout.gap}px;
  width: 100%;
`;
