import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';

import Title from '../../basic/text/Title';
import Text from '../../basic/text/Text';
import ToastNotification from '../../complex/toast/ToastNotification';
import Notification from '../../complex/toast/Notification';
import colorSchemes from '../../../constants/Colors';
import { faCheckCircle, faDotCircle, faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';

const ToastShowcase = () => {
  const theme = useSelector(themeSelector.theme);

  const showSuccess = () => ToastNotification.success({ text: 'This is a success message' });
  const showInfo = () => ToastNotification.info({ text: 'This is an info message' });
  const showWarning = () => ToastNotification.warning({ text: 'This is a warning message' });
  const showDanger = () => ToastNotification.danger({ text: 'This is a danger message' });

  return (
    <>
      <Title>Toast notifications</Title>
      <Text color={theme.secondaryText}>Success:</Text>
      <Notification
        text={'Press to trigger success notification'}
        color={'success'}
        props={{
          icon: faCheckCircle,
          onPress: showSuccess,
        }}
      />
      <Text color={theme.secondaryText}>Info:</Text>
      <Notification
        text={'Press to trigger info notification'}
        color={'main'}
        props={{
          icon: faDotCircle,
          onPress: showInfo,
        }}
      />
      <Text color={theme.secondaryText}>Warning:</Text>
      <Notification
        text={'Press to trigger warning notification'}
        color={'warning'}
        props={{
          icon: faQuestionCircle,
          onPress: showWarning,
        }}
      />
      <Text color={theme.secondaryText}>Danger:</Text>
      <Notification
        text={'Press to trigger danger notification'}
        color={'danger'}
        props={{
          icon: faExclamation,
          onPress: showDanger,
        }}
      />
    </>
  );
};

export default ToastShowcase;
