import React, { useState } from 'react';
import { IOption } from '../../basic/dropdown/Dropdown';

import Text from '../../basic/text/Text';
import Title from '../../basic/text/Title';
import LabeledDropdown from '../../basic/dropdown/LabeledDropdown';
import Avatar from '../../basic/avatar/Avatar';

import { AvatarOption, AvatarSpace, RowFlow, SampleInput } from './styled';

const options: { [key: string]: IOption[] } = {
  default: [
    { value: 1, label: <Text>Option one</Text> },
    { value: 2, label: <Text>Option two</Text> },
    { value: 3, label: <Text>Option three</Text> },
  ],
  longList: [
    { value: 1, label: <Text>One</Text> },
    { value: 2, label: <Text>Two</Text> },
    { value: 3, label: <Text>Three</Text> },
    { value: 4, label: <Text>Four</Text> },
    { value: 5, label: <Text>Five</Text> },
    { value: 6, label: <Text>Six</Text> },
    { value: 7, label: <Text>Seven</Text> },
    { value: 8, label: <Text>Eight</Text> },
    { value: 9, label: <Text>Nine</Text> },
    { value: 10, label: <Text>Ten</Text> },
  ],
  hasDisabled: [
    { value: 1, label: <Text>Option one</Text>, disabled: true },
    { value: 2, label: <Text>Option two</Text>, disabled: true },
    { value: 3, label: <Text>Option three</Text> },
    { value: 4, label: <Text>Option four</Text> },
  ],
  avatars: [
    {
      value: 1,
      label: (
        <AvatarOption>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-1.jpg')} size={'smallest'} />
          <AvatarSpace />
          <Text>Option one</Text>
        </AvatarOption>
      ),
    },
    {
      value: 2,
      label: (
        <AvatarOption>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-2.jpg')} size={'smallest'} />
          <AvatarSpace />
          <Text>Option two</Text>
        </AvatarOption>
      ),
      disabled: true,
    },
    {
      value: 3,
      label: (
        <AvatarOption>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-3.jpg')} size={'smallest'} />
          <AvatarSpace />
          <Text>Option three</Text>
        </AvatarOption>
      ),
    },
    {
      value: 4,
      label: (
        <AvatarOption>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-4.jpg')} size={'smallest'} />
          <AvatarSpace />
          <Text>Option four</Text>
        </AvatarOption>
      ),
    },
    {
      value: 5,
      label: (
        <AvatarOption>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-5.jpg')} size={'smallest'} />
          <AvatarSpace />
          <Text>Option five</Text>
        </AvatarOption>
      ),
    },
  ],
};

const DropdownShowcase = () => {
  const [defaultValue, setDefaultValue] = useState<string | number>(1);
  const [nothingValue, setNothingValue] = useState<string | number>('');
  const [longListValue, setLongListValue] = useState<string | number>(3);
  const [disabledValue, setDisabledValue] = useState<string | number>(3);
  const [errorValue, setErrorValue] = useState<string | number>(2);
  const [customValue, setCustomValue] = useState<string | number>(1);

  return (
    <>
      <Title>Dropdowns</Title>
      <LabeledDropdown
        label={'Default'}
        value={defaultValue}
        options={options.default}
        onChange={(value) => setDefaultValue(value)}
      />
      <LabeledDropdown
        label={'Nothing selected'}
        value={nothingValue}
        options={options.default}
        onChange={(value) => setNothingValue(value)}
      />
      <LabeledDropdown
        label={'Long list'}
        value={longListValue}
        options={options.longList}
        onChange={(value) => setLongListValue(value)}
      />
      <LabeledDropdown
        label={'Disabled'}
        value={2}
        options={options.default}
        onChange={(value) => setDefaultValue(value)}
        disabled={true}
      />
      <LabeledDropdown
        label={'Disabled options'}
        value={disabledValue}
        options={options.hasDisabled}
        onChange={(value) => setDisabledValue(value)}
      />
      <LabeledDropdown
        label={'With error'}
        value={errorValue}
        options={options.hasDisabled}
        onChange={(value) => setErrorValue(value)}
        errorMessage={'Error message here'}
      />
      <LabeledDropdown
        label={'Custom content'}
        value={customValue}
        options={options.avatars}
        onChange={(value) => setCustomValue(value)}
      />
    </>
  );
};

export default DropdownShowcase;
