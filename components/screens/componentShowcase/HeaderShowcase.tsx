import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';

import Title from '../../basic/text/Title';
import Text from '../../basic/text/Text';
import HeaderBar from '../../complex/headerBar/HeaderBar';
import { RootNavigationNames } from '../../../navigation/LinkingConfiguration';

const HeaderShowcase = () => {
  const theme = useSelector(themeSelector.theme);

  return (
    <>
      <Title>App header</Title>
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        Default:
      </Text>
      <HeaderBar title={'Header title'} />
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        With back button:
      </Text>
      <HeaderBar title={'Header title'} back={RootNavigationNames.ComponentShowcase} />
    </>
  );
};

export default HeaderShowcase;
