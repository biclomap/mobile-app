import styled from 'styled-components/native';
import Layout from '../../../constants/Layout';
import Avatar from '../../basic/avatar/Avatar';

export const Container = styled.ScrollView`
  padding: ${Layout.layout.gap}px;
`;

export const RowFlow = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
`;

export const SampleText = styled.View`
  margin-right: ${Layout.layout.gap}px;
`;

export const SampleToggle = styled.View`
  margin-left: ${Layout.layout.gap}px;
  margin-top: ${Layout.layout.gap}px;
`;

export const SampleInput = styled.View`
  margin-right: ${Layout.layout.gap}px;
`;

export const Gap = styled.View`
  height: ${Layout.layout.gap}px;
`;

export const AvatarSpace = styled.View`
  width: ${Math.ceil(Layout.layout.gap / 6)}px;
`;

export const AvatarOption = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
`;
