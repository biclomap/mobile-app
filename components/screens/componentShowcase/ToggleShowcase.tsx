import React, { useState } from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import { IColors, ITheme } from '../../../constants/Colors';

import themeSelector from '../../../constants/themeSelector';
import Text from '../../basic/text/Text';

import Title from '../../basic/text/Title';
import Toggle from '../../basic/toggle/Toggle';

import { RowFlow, SampleToggle } from './styled';

interface IProps {
  theme: ITheme;
  title: string;
  value: boolean;
  onChange: (value: boolean) => void;
  color?: IColors;
}

const ToggleSample = (props: IProps) => {
  const { theme, title, value, onChange, color } = props;

  return (
    <SampleToggle>
      <Text color={theme.secondaryText}>{title}</Text>
      <Toggle value={value} disabled={title === 'Disabled'} onChange={onChange} color={color} />
    </SampleToggle>
  );
};

const ToggleShowcase = () => {
  const [switches, setSwitches] = useState({
    default: true,
    disabled: true,
    main: true,
    success: true,
    danger: true,
    warning: true,
  });

  const theme = useSelector(themeSelector.theme);

  const handleChange = (key: keyof typeof switches, value: boolean) => {
    setSwitches({ ...switches, [key]: value });
  };

  return (
    <>
      <Title>Toggles</Title>
      <RowFlow>
        <ToggleSample
          theme={theme}
          title={'Default'}
          value={switches.default}
          onChange={(value) => handleChange('default', value)}
        />
        <ToggleSample
          theme={theme}
          title={'Disabled'}
          value={switches.disabled}
          onChange={(value) => handleChange('default', value)}
        />
        <ToggleSample
          theme={theme}
          title={'Main'}
          value={switches.main}
          onChange={(value) => handleChange('main', value)}
          color={'main'}
        />
        <ToggleSample
          theme={theme}
          title={'Success'}
          value={switches.success}
          onChange={(value) => handleChange('success', value)}
          color={'success'}
        />
        <ToggleSample
          theme={theme}
          title={'Danger'}
          value={switches.danger}
          onChange={(value) => handleChange('danger', value)}
          color={'danger'}
        />
        <ToggleSample
          theme={theme}
          title={'Warning'}
          value={switches.warning}
          onChange={(value) => handleChange('warning', value)}
          color={'warning'}
        />
      </RowFlow>
    </>
  );
};

export default ToggleShowcase;
