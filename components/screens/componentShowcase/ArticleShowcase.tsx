import React from 'react';

import Card from '../../basic/card/Card';
import Description from '../../basic/text/Description';
import Title from '../../basic/text/Title';

const ArticleShowcase = () => {
  return (
    <>
      <Title>Article within a Card</Title>
      <Card>
        <Title>Article title</Title>
        <Description>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus accusamus deserunt eligendi
          praesentium sapiente voluptate provident a dicta unde? Consectetur soluta beatae quisquam fuga laborum
          corporis a quos voluptatum aut?
        </Description>
      </Card>
    </>
  );
};

export default ArticleShowcase;
