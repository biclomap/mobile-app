import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';

import Title from '../../basic/text/Title';
import Text from '../../basic/text/Text';

import { RowFlow, SampleText } from './styled';

const FontShowcase = () => {
  const theme = useSelector(themeSelector.theme);

  return (
    <>
      <Title>Fonts</Title>
      <RowFlow>
        <SampleText>
          <Text size={2} type={'medium'} color={theme.mainText}>
            Medium
          </Text>
        </SampleText>
        <SampleText>
          <Text size={2} type={'medium-italic'} color={theme.mainText}>
            Medium Italic
          </Text>
        </SampleText>
        <SampleText>
          <Text size={2} type={'regular'} color={theme.mainText}>
            Regular
          </Text>
        </SampleText>
        <SampleText>
          <Text size={2} type={'regular-italic'} color={theme.mainText}>
            Regular Italic
          </Text>
        </SampleText>
        <SampleText>
          <Text size={2} type={'light'} color={theme.mainText}>
            Light
          </Text>
        </SampleText>
        <SampleText>
          <Text size={2} type={'light-italic'} color={theme.mainText}>
            Light Italic
          </Text>
        </SampleText>
      </RowFlow>
    </>
  );
};

export default FontShowcase;
