import React from 'react';

import Title from '../../basic/text/Title';
import ColorSample from '../../basic/colorSample/ColorSample';

import { RowFlow } from './styled';

const ColorShowcase = () => {
  return (
    <>
      <Title>Colors</Title>
      <RowFlow>
        <ColorSample name={'Main'} color={'main'} />
        <ColorSample name={'Success'} color={'success'} />
        <ColorSample name={'Danger'} color={'danger'} />
        <ColorSample name={'Warning'} color={'warning'} />
        <ColorSample name={'Violet'} color={'violet'} />
        <ColorSample name={'Yellow'} color={'yellow'} />
      </RowFlow>
    </>
  );
};

export default ColorShowcase;
