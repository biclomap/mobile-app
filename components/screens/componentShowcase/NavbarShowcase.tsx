import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';
import RootTabBar from '../../../navigation/RootTabBar';

import Title from '../../basic/text/Title';

const NavbarShowcase = () => {
  const theme = useSelector(themeSelector.theme);

  return (
    <>
      <Title>App navigation bar</Title>
      <RootTabBar theme={theme} />
    </>
  );
};

export default NavbarShowcase;
