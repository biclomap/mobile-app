import React from 'react';

import Title from '../../basic/text/Title';
import Text from '../../basic/text/Text';
import ThemeToggle from '../../complex/themeToggle/ThemeToggle';
import ColorShowcase from './ColorShowcase';
import FontShowcase from './FontShowcase';
import ArticleShowcase from './ArticleShowcase';
import ButtonShowcase from './ButtonShowcase';
import ToggleShowcase from './ToggleShowcase';
import AvatarShowcase from './AvatarShowcase';
import CheckboxShowcase from './CheckboxShowcase';
import InputShowcase from './InputShowcase';
import DropdownShowcase from './DropdownShowcase';
import ToastShowcase from './ToastShowcase';

import { Container, Gap } from './styled';
import HeaderShowcase from './HeaderShowcase';
import NavbarShowcase from './NavbarShowcase';

const ComponentShowcase = () => {
  return (
    <Container>
      <Title>Note:</Title>
      <Text>This screen is meant to be seen only during development.</Text>
      <Text>Will not be reachable by navigation in the final product.</Text>
      <Gap />
      <ThemeToggle />
      <Gap />
      <ColorShowcase />
      <Gap />
      <FontShowcase />
      <Gap />
      <ArticleShowcase />
      <Gap />
      <ButtonShowcase />
      <Gap />
      <ToggleShowcase />
      <Gap />
      <AvatarShowcase />
      <Gap />
      <CheckboxShowcase />
      <Gap />
      <DropdownShowcase />
      <Gap />
      <InputShowcase />
      <Gap />
      <ToastShowcase />
      <Gap />
      <HeaderShowcase />
      <Gap />
      <NavbarShowcase />
      <Gap />
      <Gap />
    </Container>
  );
};

export default ComponentShowcase;
