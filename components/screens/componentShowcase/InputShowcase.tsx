import React, { useState } from 'react';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

import Title from '../../basic/text/Title';
import LabeledInput from '../../basic/input/LabeledInput';

import { SampleInput } from './styled';

const InputShowcase = () => {
  const [value, setValue] = useState('Random text');
  const [password, setPassword] = useState('this is password');
  const [hidden, setHidden] = useState(true);

  const handleIconPress = () => setHidden(!hidden);

  return (
    <>
      <Title>Inputs</Title>
      <LabeledInput label={'Empty'} />
      <LabeledInput label={'Placeholder'} placeholder={'Placeholder text'} />
      <LabeledInput label={'Editable'} value={value} onChange={setValue} placeholder={'Editable input'} />
      <LabeledInput
        label={'Disabled'}
        value={value}
        onChange={setValue}
        placeholder={'Editable input'}
        disabled={true}
      />
      <LabeledInput
        label={'Secure'}
        value={password}
        onChange={setPassword}
        placeholder={'Like passwords'}
        secure={true}
      />
      <LabeledInput label={'With error'} value={'Ups'} errorMessage={'This here is an error message'} />
      <LabeledInput
        label={'With icon'}
        value={'now U see m3'}
        secure={hidden}
        faIcon={hidden ? faEye : faEyeSlash}
        onIconPress={handleIconPress}
      />
    </>
  );
};

export default InputShowcase;
