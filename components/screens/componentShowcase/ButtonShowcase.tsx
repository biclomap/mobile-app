import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';

import Text from '../../basic/text/Text';
import Title from '../../basic/text/Title';
import Button from '../../basic/button/Button';

import { RowFlow } from './styled';

const ButtonShowcase = () => {
  const theme = useSelector(themeSelector.theme);

  return (
    <>
      <Title>Buttons</Title>
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        With bordered:
      </Text>
      <Button title={'Default'} onPress={() => null} />
      <Button title={'Disabled'} onPress={() => null} type={'disabled'} />
      <Button title={'Main'} onPress={() => null} type={'main'} />
      <Button title={'Success'} onPress={() => null} type={'success'} />
      <Button title={'Danger'} onPress={() => null} type={'danger'} />
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        Without border:
      </Text>
      <Button title={'Default'} onPress={() => null} bordered={false} />
      <Button title={'Disabled'} onPress={() => null} type={'disabled'} bordered={false} />
      <Button title={'Main'} onPress={() => null} type={'main'} bordered={false} />
      <Button title={'Success'} onPress={() => null} type={'success'} bordered={false} />
      <Button title={'Danger'} onPress={() => null} type={'danger'} bordered={false} />
    </>
  );
};

export default ButtonShowcase;
