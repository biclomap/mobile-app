import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { ITheme } from '../../../constants/Colors';

import themeSelector from '../../../constants/themeSelector';
import Text from '../../basic/text/Text';
import CheckBox from '../../basic/checkbox/CheckBox';
import Title from '../../basic/text/Title';

import { RowFlow, SampleToggle } from './styled';

interface IProps {
  theme: ITheme;
  checked: 'checked' | 'unchecked' | 'indeterminate' | boolean;
  title: string;
  disabled?: boolean;
  onPress?: (value: boolean) => void;
}

const CheckSample = (props: IProps) => {
  const { theme, checked, title, disabled, onPress } = props;

  return (
    <SampleToggle>
      <Text color={theme.secondaryText}>{title}</Text>
      <CheckBox checked={checked} disabled={disabled} onPress={onPress} />
    </SampleToggle>
  );
};

const CheckboxShowcase = () => {
  const [checks, setChecks] = useState({
    first: true,
    second: false,
    third: 'indeterminate' as 'indeterminate' | boolean,
  });

  const theme = useSelector(themeSelector.theme);

  const handleChange = (key: keyof typeof checks, value: boolean) => {
    setChecks({ ...checks, [key]: value });
  };

  return (
    <>
      <Title>Toggles</Title>
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        Enabled:
      </Text>
      <RowFlow>
        <CheckSample
          theme={theme}
          title={'Checked'}
          checked={checks.first}
          onPress={(value) => handleChange('first', value)}
        />
        <CheckSample
          theme={theme}
          title={'Unchecked'}
          checked={checks.second}
          onPress={(value) => handleChange('second', value)}
        />
        <CheckSample
          theme={theme}
          title={'Indeterminate'}
          checked={checks.third}
          onPress={(value) => handleChange('third', value)}
        />
      </RowFlow>
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        Disabled:
      </Text>
      <RowFlow>
        <CheckSample theme={theme} title={'Checked'} checked={'checked'} disabled={true} />
        <CheckSample theme={theme} title={'Unchecked'} checked={'unchecked'} disabled={true} />
        <CheckSample theme={theme} title={'Indeterminate'} checked={'indeterminate'} disabled={true} />
      </RowFlow>
    </>
  );
};

export default CheckboxShowcase;
