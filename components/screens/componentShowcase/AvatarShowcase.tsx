import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';

import themeSelector from '../../../constants/themeSelector';

import Title from '../../basic/text/Title';
import Avatar from '../../basic/avatar/Avatar';
import Text from '../../basic/text/Text';

import { RowFlow } from './styled';

const WithMargin = styled.View`
  margin-right: 15px;
`;

const AvatarShowcase = () => {
  const theme = useSelector(themeSelector.theme);

  return (
    <>
      <Title>Avatars</Title>
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        Without image:
      </Text>
      <RowFlow>
        <WithMargin>
          <Avatar bgColor={'main'} size={'biggest'} label={'AB'} />
        </WithMargin>
        <WithMargin>
          <Avatar bgColor={'success'} size={'big'} label={'CD'} />
        </WithMargin>
        <WithMargin>
          <Avatar bgColor={'warning'} size={'regular'} label={'EF'} />
        </WithMargin>
        <WithMargin>
          <Avatar bgColor={'danger'} size={'small'} label={'GH'} />
        </WithMargin>
        <WithMargin>
          <Avatar bgColor={'violet'} size={'smallest'} label={'IJ'} />
        </WithMargin>
      </RowFlow>
      <Text type={'regular'} size={2} color={theme.secondaryText}>
        With image:
      </Text>
      <RowFlow>
        <WithMargin>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-1.jpg')} size={'biggest'} />
        </WithMargin>
        <WithMargin>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-2.jpg')} size={'big'} />
        </WithMargin>
        <WithMargin>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-3.jpg')} size={'regular'} />
        </WithMargin>
        <WithMargin>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-4.jpg')} size={'small'} />
        </WithMargin>
        <WithMargin>
          <Avatar img={require('../../../assets/images/showcase/avatar-pic-5.jpg')} size={'smallest'} />
        </WithMargin>
      </RowFlow>
    </>
  );
};

export default AvatarShowcase;
