import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import { authSelector, clearAuthState } from '../../../redux/Authentication';

import { Screen } from '../../basic/screen/Screen';
import { AppNavigationNames } from '../../../navigation/LinkingConfiguration';
import Title from '../../basic/text/Title';
import Text from '../../basic/text/Text';
import Button from '../../basic/button/Button';
import themeSelector from '../../../constants/themeSelector';

import { CenterPositioner } from './styled';
import { Gap } from '../componentShowcase/styled';

const Home = () => {
  const theme = useSelector(themeSelector.theme);
  const user = useSelector(authSelector);

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const handleFakeLogoutPress = async () => {
    await dispatch(clearAuthState());
    navigation.navigate(AppNavigationNames.Auth);
  };

  return (
    <Screen>
      <CenterPositioner>
        <Title>* This screen will be deprecated</Title>
        <Gap />
        <Text size={5}>Hello!</Text>
        <Gap />
        <Text size={3}>Welcome {user.name}</Text>
        <Gap />
        <Text size={2} color={theme.secondaryText} justify={'center'}>
          Check out the next tab to see the available components.
        </Text>
        <Gap />
        <Button type={'default'} title={'Logout'} onPress={handleFakeLogoutPress} testID={'logout-button'} />
      </CenterPositioner>
    </Screen>
  );
};

export default Home;
