import styled from 'styled-components/native';

export const CenterPositioner = styled.View`
  height: 100%;
  align-items: center;
  justify-content: center;
`;
