import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import { AppNavigationNames } from '../../../navigation/LinkingConfiguration';

import { Screen } from '../../basic/screen/Screen';
import Text from '../../basic/text/Text';
import Divider from '../../basic/text/Divider';
import LabeledInput from '../../basic/input/LabeledInput';
import Button from '../../basic/button/Button';
import Layout from '../../../constants/Layout';

import { Form } from './styled';
import { authRequestStatusSelector, cleanupRequestStatus, loginUserAction } from '../../../redux/Authentication';
import { useEffect } from 'react';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const authRequest = useSelector(authRequestStatusSelector);

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const dividerWidth = Math.ceil(Layout.window.width / 2);

  const onPressLogin = () => {
    dispatch(loginUserAction({ email, password }));
  };

  useEffect(() => {
    if (authRequest.success.includes(loginUserAction.typePrefix)) {
      navigation.navigate(AppNavigationNames.Root);
    }
    return () => {
      dispatch(cleanupRequestStatus(loginUserAction.typePrefix));
    };
  }, [dispatch, navigation, authRequest.success]);

  return (
    <Screen>
      <Form justify={'center'}>
        <LabeledInput label={'E-mail'} placeholder={'your e-mail'} value={email} onChange={setEmail} />
        <LabeledInput label={'Password'} placeholder={'your password of choice'} value={password} onChange={setPassword} secure={true} />
        <Button type={'main'} title={'Login'} onPress={onPressLogin} isLoading={authRequest.pending.includes(loginUserAction.typePrefix)} />
        <Divider width={dividerWidth} />
        <Text justify={'center'}>Right now any of inputs will act like logging in</Text>
      </Form>
    </Screen>
  );
};

export default Login;
