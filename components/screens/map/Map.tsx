import React, { useEffect, useState } from 'react';
import { Platform } from 'react-native';
import { UrlTile, MAP_TYPES, PROVIDER_DEFAULT, Region, Marker, LatLng } from 'react-native-maps';
import * as Location from 'expo-location';
import { LocationObject } from 'expo-location';
import * as Permissions from 'expo-permissions';

import You from './You';
import Controls from './Controls';

import { Container, MapContainer } from './styles';

const tileUrl = 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png';

const initialRegion: Region = {
  latitude: 45.7537,
  longitude: 21.22571,
  latitudeDelta: 0.08,
  longitudeDelta: 0.04,
};

const Map = () => {
  const [region, setRegion] = useState<Region | undefined>(initialRegion);
  const [currentLocation, setCurrentLocation] = useState<LatLng>({ latitude: 45.7537, longitude: 21.22571 });
  const [followMe, setFollowMe] = useState(true);

  let lastUpdated = 0;

  useEffect(() => {
    followMe &&
      setRegion({
        latitude: currentLocation.latitude,
        longitude: currentLocation.longitude,
        latitudeDelta: region?.latitudeDelta ?? initialRegion.latitudeDelta,
        longitudeDelta: region?.longitudeDelta ?? initialRegion.longitudeDelta,
      });
  }, [currentLocation, followMe]);

  const updateCurrentLocation = async (location: LocationObject) => {
    if (location.timestamp > lastUpdated) {
      lastUpdated = location.timestamp;
      const { coords } = location;
      setCurrentLocation({ latitude: coords.latitude, longitude: coords.longitude });
    }
  };

  useEffect(() => {
    let subscription: { remove(): void } | undefined = undefined;
    const subscribe = async () => {
      while (!(await Permissions.getAsync(Permissions.LOCATION)).granted) {
        await Permissions.askAsync(Permissions.LOCATION);
      }
      subscription = await Location.watchPositionAsync(
        { distanceInterval: 5, accuracy: Location.Accuracy.Balanced },
        updateCurrentLocation
      );
    };
    subscribe();
    return () => {
      subscription?.remove();
    };
  }, []);

  return (
    <Container>
      <MapContainer
        provider={PROVIDER_DEFAULT}
        region={region}
        onRegionChangeComplete={(region) => setRegion(region)}
        // mapType={Platform.OS == 'android' ? MAP_TYPES.NONE : MAP_TYPES.STANDARD}
      >
        {/* <UrlTile urlTemplate={tileUrl} maximumZ={19} shouldReplaceMapContent={true} /> */}
        <You coordinates={currentLocation} />
      </MapContainer>
      <Controls followMe={followMe} setFollowMe={setFollowMe} />
    </Container>
  );
};

export default Map;
