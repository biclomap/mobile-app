import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';

import Card from '../../basic/card/Card';
import Toggle from '../../basic/toggle/Toggle';
import Text from '../../basic/text/Text';

import { ControlRow, ControlsContainer } from './styles';

interface IProps {
  followMe: boolean;
  setFollowMe: (value: boolean) => void;
}

const Controls = (props: IProps) => {
  const { followMe, setFollowMe } = props;
  const theme = useSelector(themeSelector.theme);

  return (
    <ControlsContainer>
      <Card>
        <ControlRow>
          <Text color={theme.secondaryText}>Follow me</Text>
          <Toggle value={followMe} onChange={setFollowMe} />
        </ControlRow>
      </Card>
    </ControlsContainer>
  );
};

export default Controls;
