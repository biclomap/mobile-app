import styled from 'styled-components/native';
import MapView from 'react-native-maps';
import Layout from '../../../constants/Layout';

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

export const MapContainer = styled(MapView)`
  flex: 1;
  width: 100%;
  height: 100%;
`;

export const ControlsContainer = styled.View`
  position: absolute;
  opacity: 0.8;
  top: ${Layout.font.sizeFactor}px;
  right: ${Layout.font.sizeFactor}px;
`;

export const ControlRow = styled.View`
  flex-direction: row;
`;
