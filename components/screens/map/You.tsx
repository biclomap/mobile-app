import React from 'react';
import { useSelector } from 'react-redux';
import { LatLng } from 'react-native-maps';

import themeSelector from '../../../constants/themeSelector';

import Bike from '../../basic/mapMarkers/Bike';

interface IProps {
  coordinates: LatLng;
}

const You = (props: IProps) => {
  const { coordinates } = props;
  const theme = useSelector(themeSelector.theme);

  return <Bike coordinates={coordinates} color={theme.main} />;
};

export default You;
