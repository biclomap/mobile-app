import React from 'react';
import { useSelector } from 'react-redux';
import { DefaultTheme } from 'react-native-paper';
import { Checkbox as Cb } from 'react-native-paper';

import themeSelector from '../../../constants/themeSelector';

interface IProps {
  checked: 'checked' | 'unchecked' | 'indeterminate' | boolean;
  disabled?: boolean;
  onPress?: (value: boolean) => void;
}

const CheckBox = (props: IProps) => {
  const { checked, disabled, onPress } = props;

  const theme = useSelector(themeSelector.theme);

  const cbTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      disabled: theme.tabIconDefault,
    },
  };

  const status = typeof checked === 'boolean' ? (checked ? 'checked' : 'unchecked') : checked;

  return (
    // @ts-ignore: false error
    <Cb.Android
      status={status}
      color={theme.success}
      disabled={disabled}
      theme={theme.scheme === 'dark' ? cbTheme : undefined}
      uncheckedColor={theme.scheme === 'dark' ? theme.mainText : undefined}
      onPress={() => !disabled && onPress && onPress(status === 'unchecked')}
    />
  );
};

export default CheckBox;
