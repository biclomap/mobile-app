import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';

import themeSelector from '../../../constants/themeSelector';

import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';

type IJustify = 'baseline' | 'center' | 'flex-start' | 'flex-end' | 'stretch';

const Box = styled.View<{ theme: ITheme; width: string; alignItems: IJustify }>`
  padding: ${Layout.layout.gap}px;
  margin: 5px;
  background-color: ${(props) => props.theme.card};
  width: ${(props) => props.width};
  align-items: ${(props) => props.alignItems};

  shadow-color: #888;
  shadow-offset: 0 10px;
  shadow-opacity: 0.8;
  shadow-radius: 10px;
  elevation: 2;
`;

interface IProps {
  width?: number | string;
  testID?: string;
  justify?: IJustify;
}

const Card: FunctionComponent<IProps> = (props) => {
  const { children, width = 'auto', justify = 'baseline', testID } = props;

  const theme = useSelector(themeSelector.theme);

  return (
    <Box theme={theme} width={typeof width === 'number' ? `${width}px` : width} alignItems={justify} testID={testID}>
      {children}
    </Box>
  );
};

export default Card;
