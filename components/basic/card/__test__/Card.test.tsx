import React from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';

import colorSchemes from '../../../../constants/Colors';

import Card from '../Card';

const mockStore = configureStore();

describe('Card component', () => {
  const testData = {
    id: 'card',
    width: 250,
    children: <Text>Some text</Text>,
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
    component = renderer.create(
      <Provider store={store}>
        <Card testID={testData.id} width={testData.width}>
          {testData.children}
        </Card>
      </Provider>
    ).root;
  });

  it('renders the component', () => {
    expect(component.findAll((el) => el.type === View && el.props.testID === testData.id).length).toEqual(1);
  });

  it('renders with correct color', () => {
    expect(component.findByType(View).props.style[0].backgroundColor).toEqual(colorSchemes.light.card);
  });

  it('renders with correct width', () => {
    expect(component.findByType(View).props.style[0].width).toEqual(testData.width);
  });

  it('renders with provided children', () => {
    expect(component.findByType(View).props.children).toEqual(testData.children);
  });
});
