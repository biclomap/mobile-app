import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';

import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';
import themeSelector from '../../../constants/themeSelector';

interface IProps {
  width: number;
}

const HorizontalLine = styled.View<{ theme: ITheme; width: number }>`
  height: 1px;
  width: ${(props) => props.width}px;
  background-color: ${(props) => props.theme.inputBorder};
  margin: ${Math.ceil(Layout.layout.gap)}px ${Math.ceil(Layout.layout.gap / 3)}px;
`;

const Divider = (props: IProps) => {
  const { width } = props;

  const theme = useSelector(themeSelector.theme);

  return <HorizontalLine theme={theme} width={width} />;
};

export default Divider;
