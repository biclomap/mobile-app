import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';

import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';
import themeSelector from '../../../constants/themeSelector';

const StyledTitle = styled.Text<{ theme: ITheme; size: number; justify: string }>`
  font-family: 'medium';
  font-size: ${(props) => props.size}px;
  color: ${(props) => props.theme.mainText};
  text-align: ${(props) => props.justify};
  margin-bottom: ${Layout.layout.gap}px;
`;

interface IProps {
  justify?: 'auto' | 'left' | 'right' | 'center' | 'justify';
}

const Title: FunctionComponent<IProps> = (props) => {
  const { children, justify = 'auto' } = props;

  const theme = useSelector(themeSelector.theme);

  return (
    <StyledTitle theme={theme} size={Layout.font.sizeFactor * 3} justify={justify}>
      {children}
    </StyledTitle>
  );
};

export default Title;
