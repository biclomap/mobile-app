import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';

import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';
import themeSelector from '../../../constants/themeSelector';

const StyledDescription = styled.Text<{ theme: ITheme; size: number }>`
  font-family: 'regular';
  font-size: ${(props) => props.size}px;
  color: ${(props) => props.theme.mainText};
`;

const Description: FunctionComponent = (props) => {
  const theme = useSelector(themeSelector.theme);

  return (
    <StyledDescription theme={theme} size={Layout.font.sizeFactor * 2}>
      {props.children}
    </StyledDescription>
  );
};

export default Description;
