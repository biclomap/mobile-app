import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';
import Layout from '../../../constants/Layout';

import themeSelector from '../../../constants/themeSelector';

type FontTypes = 'regular' | 'regular-italic' | 'medium' | 'medium-italic' | 'light' | 'light-italic';

interface IProps {
  size?: number;
  type?: FontTypes;
  color?: string;
  justify?: 'auto' | 'left' | 'right' | 'center' | 'justify';
  testID?: string;
}

export const StyledText = styled.Text<IProps>`
  font-family: ${(props) => props.type || 'regular'};
  font-size: ${(props) => props.size || Layout.font.sizeFactor * 2}px;
  color: ${(props) => props.color || 'black'};
  text-align: ${(props) => props.justify};
`;

/**
 *
 * @param size - a factor of the base font size
 */
const Text: FunctionComponent<IProps> = (props) => {
  const { children, color, size, type, justify = 'auto', testID } = props;

  const theme = useSelector(themeSelector.theme);

  const fontSize = size ? size * Layout.font.sizeFactor : 2 * Layout.font.sizeFactor;

  return (
    <StyledText size={fontSize} type={type} color={color || theme.mainText} justify={justify} testID={testID}>
      {children}
    </StyledText>
  );
};

export default Text;
