import React from 'react';
import { Text as NativeText } from 'react-native';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';

import colorSchemes from '../../../../constants/Colors';
import Layout from '../../../../constants/Layout';

import Text, { StyledText } from '../Text';

const mockStore = configureStore();

describe('Text component', () => {
  const testData = {
    id: 'text',
    children: 'Some text',
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
    component = renderer.create(
      <Provider store={store}>
        <Text testID={testData.id}>{testData.children}</Text>
      </Provider>
    ).root;
  });

  it('renders the component', () => {
    expect(component.findAll((el) => el.type === StyledText && el.props.testID === testData.id).length).toEqual(1);
  });

  it('renders with correct color', () => {
    expect(component.findByType(NativeText).props.style[0].color).toEqual(colorSchemes.light.mainText);
  });

  it('renders with provided children', () => {
    expect(component.findByType(StyledText).props.children).toEqual(testData.children);
  });
});

describe('Text component - custom', () => {
  const testData = {
    children: 'Some text',
    size: 3,
    color: '#123456',
    fontType: 'medium' as 'medium',
    justify: 'center' as 'center',
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
    component = renderer.create(
      <Provider store={store}>
        <Text size={testData.size} color={testData.color} type={testData.fontType} justify={testData.justify}>
          {testData.children}
        </Text>
      </Provider>
    ).root;
  });

  it('renders with correct size', () => {
    expect(component.findByType(NativeText).props.style[0].fontSize).toEqual(testData.size * Layout.font.sizeFactor);
  });

  it('renders with correct color', () => {
    expect(component.findByType(NativeText).props.style[0].color).toEqual(testData.color);
  });

  it('renders with correct font type', () => {
    expect(component.findByType(NativeText).props.style[0].fontFamily).toEqual(testData.fontType);
  });

  it('renders with correct alignment', () => {
    expect(component.findByType(NativeText).props.style[0].textAlign).toEqual(testData.justify);
  });
});
