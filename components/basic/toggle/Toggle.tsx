import React from 'react';
import { Switch } from 'react-native';
import { useSelector } from 'react-redux';
import { IColors } from '../../../constants/Colors';
import themeSelector from '../../../constants/themeSelector';
import { DarkenColor } from '../button/styled';

interface IProps {
  value: boolean;
  onChange?: (value: boolean) => void;
  color?: IColors;
  disabled?: boolean;
}

const Toggle = (props: IProps) => {
  const { value, onChange, color = 'main', disabled = false } = props;
  const theme = useSelector(themeSelector.theme);

  const disabledColor = '#cccccc';

  return (
    <Switch
      value={value}
      trackColor={{
        false: disabled ? disabledColor : '#dddddd',
        true: disabled ? disabledColor : DarkenColor(theme[color], -60),
      }}
      thumbColor={value ? (disabled ? disabledColor : theme[color]) : theme.grey50}
      onValueChange={onChange}
      disabled={disabled}
    />
  );
};

export default Toggle;
