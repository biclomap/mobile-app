import React from 'react';
import { useSelector } from 'react-redux';
import { Avatar as Av } from 'react-native-paper';

import themeSelector from '../../../constants/themeSelector';
import { IColors } from '../../../constants/Colors';

interface IProps {
  label?: string;
  size?: keyof typeof avatarSize;
  bgColor?: IColors;
  img?: any;
}

const avatarSize = {
  biggest: 100,
  big: 80,
  regular: 60,
  small: 38,
  smallest: 28,
};

const Avatar = (props: IProps) => {
  const { label = '', size = 'regular', bgColor = 'main', img } = props;

  const theme = useSelector(themeSelector.theme);

  const style = {
    backgroundColor: theme[bgColor],
  };

  return img ? (
    // @ts-ignore: false positive
    <Av.Image source={img} size={avatarSize[size]} />
  ) : (
    // @ts-ignore: false positive
    <Av.Text label={label} size={avatarSize[size]} style={style} />
  );
};

export default Avatar;
