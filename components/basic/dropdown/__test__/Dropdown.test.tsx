import React from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import renderer, { act } from 'react-test-renderer';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';

import colorSchemes from '../../../../constants/Colors';

import Dropdown, { IOption } from '../Dropdown';

jest.mock('@fortawesome/react-native-fontawesome', () => ({
  FontAwesomeIcon: '',
}));

const mockStore = configureStore();

describe('Dropdown component', () => {
  const testData = {
    id: 'dropdown',
    width: 250,
    value: 1,
    options: [
      { value: 1, label: <Text>one</Text> },
      { value: 2, label: <Text>two</Text> },
      { value: 3, label: <Text>three</Text> },
      { value: 4, label: <Text>four</Text> },
    ] as IOption[],
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
    component = renderer.create(
      <Provider store={store}>
        <Dropdown value={testData.value} options={testData.options} testID={testData.id} width={testData.width} />
      </Provider>
    ).root;
  });

  it('renders the component', () => {
    expect(component.findAll((el) => el.type === View && el.props.testID === testData.id).length).toEqual(1);
  });

  it('starts collapsed', () => {
    expect(component.findAll((el) => el.type === View && el.props.testID === 'main-container').length).toEqual(1);
    expect(component.findAll((el) => el.type === View && el.props.testID === 'options-list').length).toEqual(0);
  });

  it('does not show error message', () => {
    expect(component.findByProps({ testID: 'error-message' }).props.children).toEqual(undefined);
  });

  it('has the correct width', () => {
    expect(
      component.find((el) => el.type === View && el.props.testID === 'main-container').props.style[0].width
    ).toEqual(testData.width);
  });

  it('has the correct value selected', () => {
    expect(component.find((el) => el.type === View && el.props.testID === 'main-container').props.children[0]).toEqual(
      testData.options[0].label
    );
  });
});

describe('Dropdown component interaction', () => {
  const testData = {
    id: 'dropdown',
    width: 250,
    value: 1,
    options: [
      { value: 1, label: <Text>one</Text> },
      { value: 2, label: <Text>two</Text> },
      { value: 3, label: <Text>three</Text> },
      { value: 4, label: <Text>four</Text> },
    ] as IOption[],
    errorMessage: 'test error message',
    function: jest.fn(),
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
    component = renderer.create(
      <Provider store={store}>
        <Dropdown
          value={testData.value}
          options={testData.options}
          onChange={testData.function}
          testID={testData.id}
          errorMessage={testData.errorMessage}
        />
      </Provider>
    ).root;
  });

  it('displays the correct error message', () => {
    expect(component.findAll((el) => el.type === Text && el.props.testID === 'error-message').length).toEqual(1);
    expect(component.find((el) => el.type === Text && el.props.testID === 'error-message').props.children).toEqual(
      testData.errorMessage
    );
  });

  it('expands and contracts dropdown list', () => {
    expect(component.findAll((el) => el.type === View && el.props.testID === 'options-list').length).toEqual(0);
    act(() => {
      component.find((el) => el.type === View && el.props.testID === 'touchable').props.onClick();
    });
    expect(component.findAll((el) => el.type === View && el.props.testID === 'options-list').length).toEqual(1);
    act(() => {
      component.find((el) => el.type === View && el.props.testID === 'touchable').props.onClick();
    });
    expect(component.findAll((el) => el.type === View && el.props.testID === 'options-list').length).toEqual(0);
  });

  it('selected item', () => {
    act(() => {
      component.find((el) => el.type === View && el.props.testID === 'touchable').props.onClick();
    });
    act(() => {
      component
        .find((el) => el.type === View && el.props.testID === 'options-list')
        .props.children.props.children[1].props.onPress();
    });
    expect(component.findAll((el) => el.type === View && el.props.testID === 'options-list').length).toEqual(0);
    expect(testData.function).toBeCalledTimes(1);
    expect(testData.function).toBeCalledWith(testData.options[1].value);
  });
});
