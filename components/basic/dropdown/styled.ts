import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import styled, { css } from 'styled-components/native';
import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';

interface IContainerProps {
  theme: ITheme;
  disabled?: boolean;
  width?: number;
  hasError?: boolean;
}

const BaseContainer = css<IContainerProps>`
  border-width: 1px;
  border-color: ${(props) => (props.hasError ? props.theme.danger : props.theme.inputBorder || 'grey')};
  border-radius: ${Layout.layout.roundness}px;
  ${(props) => (props.width ? `width: ${props.width}px;` : 'width: 100%')};

  shadow-color: #888;
  shadow-offset: 0 3px;
  shadow-opacity: 0.1;
  shadow-radius: 3px;
  elevation: 0.5;
`;

export const Container = styled.View<IContainerProps>`
  ${BaseContainer}
  background-color: ${(props) => (props.disabled ? props.theme.background : props.theme.card || 'white')};
  padding: 0 ${Layout.font.sizeFactor * 2}px;
  height: ${Layout.layout.inputHeight}px;
  justify-content: center;
  overflow: hidden;
`;

export const DisabledOverlay = styled.View<IContainerProps>`
  position: absolute;
  background-color: ${(props) => props.theme.background};
  opacity: 0.5;
  height: ${Math.ceil(Layout.font.sizeFactor * 5)}px;
  width: ${(props) => props.width || Layout.layout.gap * 5}px;
`;

export const StyledIconPositioner = styled.View`
  align-self: flex-end;
  position: absolute;
  padding: ${Layout.font.sizeFactor}px ${Layout.font.sizeFactor * 1.5}px;
  height: ${Layout.font.sizeFactor * 5}px;
  justify-content: center;
`;

export const Icon = styled.View`
  height: 90%;
  z-index: 100;
`;

export const IconPart = styled(FontAwesomeIcon)`
  position: absolute;
  top: ${Math.ceil(Layout.font.sizeFactor / 1)}px;
`;

interface IOptionsProps extends IContainerProps {
  offset: number;
}

export const OptionsContainer = styled.View<IOptionsProps>`
  ${BaseContainer}
  position: absolute;
  max-height: ${Layout.font.sizeFactor * 30}px;
  top: ${(props) => props.offset + Layout.font.sizeFactor}px;
  z-index: 900;
  overflow: hidden;
`;

interface IOption {
  theme: ITheme;
  first: boolean;
  disabled?: boolean;
}

export const Option = styled.TouchableHighlight<IOption>`
  padding: 0 ${Layout.font.sizeFactor * 2}px;
  background-color: ${(props) => (props.disabled ? props.theme.background : props.theme.card || 'white')};
  height: ${Math.ceil(Layout.font.sizeFactor * 5)}px;
  justify-content: center;
  overflow: hidden;
  ${(props) =>
    props.first
      ? `border-top-left-radius: ${Layout.layout.roundness}px; border-top-right-radius: ${Layout.layout.roundness}px;`
      : `border-bottom-left-radius: ${Layout.layout.roundness}px; border-bottom-right-radius: ${Layout.layout.roundness}px;`}

  ${(props) => !props.first && 'border-top-width: 1px;'}
  ${(props) => !props.first && `border-top-color: ${props.theme.inputBorder || 'grey'};`}
`;

export const DisabledOptionOverlay = styled.View<IContainerProps>`
  position: absolute;
  background-color: ${(props) => props.theme.background};
  opacity: 0.5;
  height: ${Math.ceil(Layout.font.sizeFactor * 5)}px;
  width: ${(props) => props.width || Layout.layout.gap * 5}px;
`;
