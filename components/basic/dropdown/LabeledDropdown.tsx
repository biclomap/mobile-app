import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';

import Text from '../text/Text';
import Dropdown, { IProps as InputProps } from '../dropdown/Dropdown';

import { Label, LabeledContainer } from '../input/styled';

interface IProps extends InputProps {
  label: string;
}

const LabeledDropdown = (props: IProps) => {
  const { label, ...rest } = props;

  const theme = useSelector(themeSelector.theme);

  return (
    <LabeledContainer width={rest.width}>
      <Label>
        <Text size={1.5} color={theme.secondaryText}>
          {label.toUpperCase()}
        </Text>
      </Label>
      <Dropdown {...rest} />
    </LabeledContainer>
  );
};

export default LabeledDropdown;
