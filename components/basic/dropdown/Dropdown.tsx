import React, { useRef, useState } from 'react';
import { LayoutChangeEvent, View, TouchableHighlight, ScrollView, UIManager, findNodeHandle } from 'react-native';
import { useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';

import themeSelector from '../../../constants/themeSelector';

import Text from '../text/Text';
import Layout from '../../../constants/Layout';

import { ErrorMessage } from '../input/styled';
import {
  Container,
  Icon,
  IconPart,
  OptionsContainer,
  Option,
  StyledIconPositioner,
  DisabledOverlay,
  DisabledOptionOverlay,
} from './styled';

export interface IOption {
  value: string | number;
  label: React.ReactElement;
  disabled?: boolean;
}

export interface IProps {
  value: string | number;
  options: IOption[];
  onChange?: (value: string | number) => void;
  callToActionText?: string;
  disabled?: boolean;
  errorMessage?: string;
  width?: number;
  testID?: string;
}

const Dropdown = (props: IProps) => {
  const { value, options, onChange, callToActionText, errorMessage, disabled, width, testID } = props;

  const [showOptions, setShowOptions] = useState(false);
  const [containerHeight, setContainerHeight] = useState(0);
  const [dropdownOffset, setDropdownOffset] = useState(0);

  const theme = useSelector(themeSelector.theme);

  const componentRef = useRef<any>(null);

  const touchable = {
    underlay: theme.scheme === 'light' ? '#eeeeee' : '#333333',
    opacity: 0.9,
  };

  const getOptionsListHeight = (event: LayoutChangeEvent) => {
    const optionsListHeight = event.nativeEvent.layout.height;
    componentRef.current.measureInWindow((x: number, y: number, width: number, height: number) => {
      if (y + optionsListHeight + height + Layout.layout.gap * 2.5 > Layout.window.height) {
        setDropdownOffset(-optionsListHeight - Layout.layout.gap);
      } else {
        setDropdownOffset(containerHeight);
      }
    });
  };

  const handleOptionSelect = (value: string | number) => {
    onChange && onChange(value);
    setShowOptions(false);
  };

  const InputIcon = (
    <StyledIconPositioner>
      <Icon>
        <FontAwesomeIcon icon={faCaretUp} size={Layout.font.sizeFactor * 1.5} color={theme.secondaryText} />
        <IconPart icon={faCaretDown} size={Layout.font.sizeFactor * 1.5} color={theme.secondaryText} />
      </Icon>
    </StyledIconPositioner>
  );

  const OptionsList = (
    <OptionsContainer
      theme={theme}
      width={width}
      offset={dropdownOffset}
      testID={'options-list'}
      onLayout={getOptionsListHeight}
    >
      <ScrollView keyboardShouldPersistTaps={'always'} nestedScrollEnabled={true}>
        {options.map((item, index) => (
          <Option
            key={item.value}
            theme={theme}
            first={index === 0}
            underlayColor={touchable.underlay}
            activeOpacity={touchable.opacity}
            onPress={() => !item.disabled && handleOptionSelect(item.value)}
            disabled={item.disabled}
          >
            <>
              <Text>{item.label}</Text>
              {item.disabled && <DisabledOptionOverlay theme={theme} width={width} />}
            </>
          </Option>
        ))}
      </ScrollView>
    </OptionsContainer>
  );

  const getLayoutInfo = (event: LayoutChangeEvent) => setContainerHeight(Math.ceil(event.nativeEvent.layout.height));

  const toggleOptions = () => !disabled && setShowOptions(!showOptions);

  return (
    <View testID={testID}>
      <TouchableHighlight
        underlayColor={touchable.underlay}
        activeOpacity={touchable.opacity}
        onPress={toggleOptions}
        testID={'touchable'}
      >
        <Container
          theme={theme}
          disabled={disabled}
          onLayout={getLayoutInfo}
          width={width}
          hasError={!!errorMessage}
          testID={'main-container'}
          ref={componentRef}
        >
          {options.find((item) => item.value === value)?.label || (
            <Text color={theme.inputPlaceholder}>{callToActionText || 'Select an option'}</Text>
          )}
          {disabled && <DisabledOverlay theme={theme} width={width} />}
          {InputIcon}
        </Container>
      </TouchableHighlight>
      <ErrorMessage>
        <Text size={1.5} color={theme.danger} testID={'error-message'}>
          {errorMessage}
        </Text>
      </ErrorMessage>
      {showOptions && OptionsList}
    </View>
  );
};

export default Dropdown;
