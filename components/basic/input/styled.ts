import styled from 'styled-components/native';
import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';

interface IStyledInputProps {
  theme: ITheme;
  focused: boolean;
  disabled?: boolean;
  hasError?: boolean;
  width?: number;
}

export const StyledInput = styled.TextInput<IStyledInputProps>`
  border-width: 1px;
  border-color: ${(props) =>
    props.hasError ? props.theme.danger : props.focused ? props.theme.main : props.theme.inputBorder || 'grey'};
  border-radius: ${Layout.layout.roundness}px;
  color: ${(props) => (props.disabled ? props.theme.secondaryText : props.theme.mainText)};
  padding: ${Math.ceil(Layout.font.sizeFactor / 2)}px ${Layout.font.sizeFactor * 2}px;
  background-color: ${(props) => (props.disabled ? props.theme.background : props.theme.card || 'white')};
  height: ${Layout.layout.inputHeight}px;
  ${(props) => (props.width ? `width: ${props.width}px;` : '')};

  shadow-color: #888;
  shadow-offset: 0 3px;
  shadow-opacity: 0.1;
  shadow-radius: 3px;
  elevation: 0.3;
`;

export const ErrorMessage = styled.View`
  margin-top: ${Math.ceil(Layout.layout.gap / 7)}px;
`;

export const Label = styled.View`
  margin-bottom: ${Math.ceil(Layout.layout.gap / 5)}px;
`;

export const LabeledContainer = styled.View<{ width?: number }>`
  width: ${(props) => (props.width ? `${props.width}px` : '100%')};
`;
