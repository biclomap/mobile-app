import React, { useState } from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

import themeSelector from '../../../constants/themeSelector';
import Text from '../text/Text';

import { ErrorMessage, StyledInput } from './styled';
import styled from 'styled-components/native';
import Layout from '../../../constants/Layout';

export interface IProps {
  value?: string | number;
  onChange?: (text: string) => void;
  placeholder?: string;
  secure?: boolean;
  faIcon?: IconDefinition;
  onIconPress?: () => void;
  disabled?: boolean;
  errorMessage?: string;
  width?: number;
}

const StyledIconPositioner = styled.View`
  align-self: flex-end;
  position: absolute;
`;

const StyledTouchableArea = styled.View`
  height: ${Layout.layout.inputHeight}px;
  justify-content: center;
  padding: ${Layout.font.sizeFactor}px;
`;

const Input = (props: IProps) => {
  const { value, onChange, placeholder, secure, faIcon, onIconPress, disabled, errorMessage, width } = props;

  const [focused, setFocused] = useState(false);

  const theme = useSelector(themeSelector.theme);

  const InputIcon = faIcon && (
    <StyledIconPositioner>
      <TouchableNativeFeedback onPress={() => onIconPress && onIconPress()}>
        <StyledTouchableArea>
          <FontAwesomeIcon icon={faIcon} size={Layout.font.sizeFactor * 2} color={theme.secondaryText} />
        </StyledTouchableArea>
      </TouchableNativeFeedback>
    </StyledIconPositioner>
  );

  return (
    <View>
      <StyledInput
        value={value?.toString()}
        onChangeText={(value) => !disabled && onChange && onChange(value)}
        theme={theme}
        focused={focused}
        placeholder={placeholder}
        placeholderTextColor={theme.inputPlaceholder}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
        showSoftInputOnFocus={!disabled}
        secureTextEntry={secure}
        editable={!disabled}
        disabled={disabled}
        hasError={!!errorMessage}
        width={width}
      />
      {InputIcon}
      <ErrorMessage>
        <Text size={1.5} color={theme.danger}>
          {errorMessage}
        </Text>
      </ErrorMessage>
    </View>
  );
};

export default Input;
