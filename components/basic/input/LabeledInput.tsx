import React from 'react';
import { useSelector } from 'react-redux';

import themeSelector from '../../../constants/themeSelector';

import Text from '../text/Text';
import Input, { IProps as InputProps } from './Input';

import { Label, LabeledContainer } from './styled';

interface IProps extends InputProps {
  label: string;
}

const LabeledInput = (props: IProps) => {
  const { label, ...rest } = props;

  const theme = useSelector(themeSelector.theme);

  return (
    <LabeledContainer>
      <Label>
        <Text size={1.5} color={theme.secondaryText}>
          {label.toUpperCase()}
        </Text>
      </Label>
      <Input {...rest} />
    </LabeledContainer>
  );
};

export default LabeledInput;
