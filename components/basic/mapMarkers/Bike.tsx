import React from 'react';
import { useSelector } from 'react-redux';
import { Marker, LatLng } from 'react-native-maps';
import Svg, { Circle, Path } from 'react-native-svg';

import themeSelector from '../../../constants/themeSelector';

interface IProps {
  coordinates: LatLng;
  color?: string;
}

const You = (props: IProps) => {
  const { coordinates, color } = props;
  const theme = useSelector(themeSelector.theme);

  return (
    <Marker coordinate={coordinates} title={'You'} style={{ height: 50, width: 50 }}>
      <Svg height={'100%'} width={'100%'} viewBox={'0 0 128 128'} fill={color || theme.secondaryText}>
        <Circle cx={'64'} cy={'52'} r={'40'} fill={theme.background} />
        <Circle cx={'44'} cy={'60'} r={'8'} />
        <Circle cx={'84'} cy={'60'} r={'8'} />
        <Path
          d={
            'M64,0A52,52,0,0,0,46,100.8l14.68,25.27a4,4,0,0,0,6.64,0L82,100.8A52,52,0,0,0,64,0ZM84,76a16,16,0,0,1-9.38-28.93l-.4-1-15.4,8a16,16,0,1,1-7.3-8.12L47.5,36H45a4,4,0,0,1,0-8H57a4,4,0,0,1,0,8h-.87l3.5,8.59,11.65-6-2-5.1A4,4,0,0,1,73,28h8a4,4,0,0,1,0,8H78.87l3.19,8.13A16,16,0,1,1,84,76Z'
          }
        />
      </Svg>
    </Marker>
  );
};

export default You;
