import styled from 'styled-components/native';

import Layout from '../../../constants/Layout';

export const Color = styled.View<{ color: string }>`
  height: 30px;
  width: 90px;
  border-radius: ${Layout.layout.roundness}px;
  margin-right: ${Layout.layout.gap}px;
  background-color: ${(props) => props.color};
`;
