import React from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';

import { IColors } from '../../../constants/Colors';
import themeSelector from '../../../constants/themeSelector';

import Text from '../text/Text';

import { Color } from './styled';

interface IProps {
  name: string;
  color: IColors;
}

const ColorSample = (props: IProps) => {
  const { name, color } = props;

  const theme = useSelector(themeSelector.theme);

  return (
    <View>
      <Text size={1} type={'light-italic'} color={theme.secondaryText}>
        {name}
      </Text>
      <Color color={theme[color]} />
    </View>
  );
};

export default ColorSample;
