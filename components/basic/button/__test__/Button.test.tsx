import React from 'react';
import Svg, { Stop } from 'react-native-svg';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';

import colorSchemes from '../../../../constants/Colors';

import Button from '../Button';
import { ButtonText, LoadingIcon } from '../styled';

jest.mock('react-native-gesture-handler', () => {});

const mockStore = configureStore();

describe('Button component', () => {
  const testData = {
    title: 'Test title',
    id: 'svg-button',
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
    component = renderer.create(
      <Provider store={store}>
        <Button title={testData.title} testID={testData.id} />
      </Provider>
    ).root;
  });

  it('it renders the button', () => {
    // need to check for both type test ID -> there are additional elements created in the background
    expect(component.findAll((el) => el.type === Svg && el.props.testID === testData.id).length).toEqual(1);
  });

  it('renders the correct text', () => {
    expect(component.findByType(ButtonText).props.children).toEqual(testData.title);
  });

  it('does not render loading animation', () => {
    expect(component.findAllByType(LoadingIcon).length).toEqual(0);
  });

  it('renders correct color', () => {
    expect(component.findAllByType(Stop).length).toEqual(2);
    expect(component.findAllByType(Stop)[0].props.stopColor).toEqual(colorSchemes.light.card);
  });
});

describe('Button component loading', () => {
  const testData = {
    title: 'Test title',
    id: 'svg-button',
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
    component = renderer.create(
      <Provider store={store}>
        <Button title={testData.title} testID={testData.id} isLoading={true} />
      </Provider>
    ).root;
  });

  it('renders the loading overlay animation', () => {
    expect(component.findAllByType(LoadingIcon).length).toEqual(1);
  });
});

describe('Button component color', () => {
  const testData = {
    title: 'Test title',
    id: 'svg-button',
  };

  let store: MockStoreEnhanced<unknown, {}>;
  let component: renderer.ReactTestInstance;

  beforeEach(() => {
    store = mockStore({
      ThemeReducer: colorSchemes.light,
    });
  });

  it('renders the correct color', () => {
    component = renderer.create(
      <Provider store={store}>
        <Button title={testData.title} testID={testData.id} type={'main'} />
      </Provider>
    ).root;
    expect(component.findAllByType(Stop)[0].props.stopColor).toEqual(colorSchemes.light.main);
  });

  it('renders the correct color', () => {
    component = renderer.create(
      <Provider store={store}>
        <Button title={testData.title} testID={testData.id} type={'success'} />
      </Provider>
    ).root;
    expect(component.findAllByType(Stop)[0].props.stopColor).toEqual(colorSchemes.light.success);
  });

  it('renders the correct color', () => {
    component = renderer.create(
      <Provider store={store}>
        <Button title={testData.title} testID={testData.id} type={'danger'} />
      </Provider>
    ).root;
    expect(component.findAllByType(Stop)[0].props.stopColor).toEqual(colorSchemes.light.danger);
  });

  it('renders the correct color', () => {
    component = renderer.create(
      <Provider store={store}>
        <Button title={testData.title} testID={testData.id} type={'disabled'} />
      </Provider>
    ).root;
    expect(component.findAllByType(Stop)[0].props.stopColor).toEqual('#fefeff');
  });
});
