import React from 'react';
import { useSelector } from 'react-redux';
import Svg, { Defs, LinearGradient, Rect, Stop } from 'react-native-svg';

import themeSelector from '../../../constants/themeSelector';

import { ButtonText, Container, DarkenColor, IType, LoadingIcon } from './styled';

interface IProps {
  title: string;
  onPress?: () => void;
  type?: IType;
  bordered?: boolean;
  isLoading?: boolean;
  width?: number;
  testID?: string;
}

const Button = (props: IProps) => {
  const { title, type = 'default', onPress, bordered = true, isLoading = false, width, testID } = props;

  const theme = useSelector(themeSelector.theme);

  const buttonTheme: {
    [key in IType]: {
      color: string;
      amount: number;
      text: string;
      underlay: string;
      pressOpacity: number;
    };
  } = {
    disabled: {
      color: '#fefeff',
      amount: 0,
      text: theme.mainText,
      underlay: '#ffffff',
      pressOpacity: 1,
    },
    default: {
      color: theme.card,
      amount: 5,
      text: theme.mainText,
      underlay: '#dddddd',
      pressOpacity: 0.9,
    },
    main: {
      color: theme.main,
      amount: 10,
      text: 'white',
      underlay: '#000000',
      pressOpacity: 0.9,
    },
    success: {
      color: theme.success,
      amount: 10,
      text: 'white',
      underlay: '#000000',
      pressOpacity: 0.9,
    },
    danger: {
      color: theme.danger,
      amount: 10,
      text: 'white',
      underlay: '#000000',
      pressOpacity: 0.9,
    },
  };

  return (
    <Container
      theme={theme}
      type={type}
      width={width}
      bordered={bordered}
      onPress={() => onPress && onPress()}
      underlayColor={buttonTheme[type].underlay}
      activeOpacity={buttonTheme[type].pressOpacity}
    >
      <Svg height='40' width='102%' testID={testID}>
        <Defs>
          <LinearGradient id='grad' x1='100%' y1='0%' x2='100%' y2='100%'>
            <Stop offset='0%' stopColor={buttonTheme[type].color} />
            <Stop offset='100%' stopColor={DarkenColor(buttonTheme[type].color, buttonTheme[type].amount)} />
          </LinearGradient>
        </Defs>

        <Rect x='0' y='0' width='100%' height='100%' fill='url(#grad)' />
        <ButtonText color={buttonTheme[type].text} hasBorders={bordered}>
          {title}
        </ButtonText>
        {isLoading && <LoadingIcon color={buttonTheme[type].text} />}
      </Svg>
    </Container>
  );
};

export default Button;
