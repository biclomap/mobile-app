import styled from 'styled-components/native';

import { ITheme } from '../../../constants/Colors';
import Layout from '../../../constants/Layout';

export const DarkenColor = (color: string, percent: number) => {
  const num = parseInt(color.replace('#', ''), 16),
    amt = Math.round(2.55 * percent),
    R = (num >> 16) - amt,
    B = ((num >> 8) & 0x00ff) - amt,
    G = (num & 0x0000ff) - amt;
  return (
    '#' +
    (
      0x1000000 +
      (R < 255 ? (R < 1 ? 0 : R) : 255) * 0x10000 +
      (B < 255 ? (B < 1 ? 0 : B) : 255) * 0x100 +
      (G < 255 ? (G < 1 ? 0 : G) : 255)
    )
      .toString(16)
      .slice(1)
  );
};

export type IType = 'default' | 'disabled' | 'main' | 'success' | 'danger';

const buttonColor = (theme: ITheme, type: IType) => {
  switch (type) {
    case 'default':
    case 'disabled':
      return theme.grey50;
    case 'main':
    case 'success':
    case 'danger':
      return DarkenColor(theme[type], 15);
    default:
      return 'white';
  }
};

interface IContainer {
  theme: ITheme;
  type: IType;
  onPress?: () => void;
  underlayColor?: string;
  activeOpacity?: number;
  bordered?: boolean;
  width?: number;
}

export const Container = styled.TouchableHighlight<IContainer>`
  margin: ${Math.ceil(Layout.layout.gap / 4)}px 0;
  border-radius: ${Layout.layout.roundness}px;
  height: ${Layout.layout.inputHeight}px;
  overflow: hidden;
  width: ${(props) => (props.width ? `${props.width}px;` : '100%')}
  ${(props) =>
    props.bordered
      ? `border: solid 1px;
  border-color: ${buttonColor(props.theme, props.type)};`
      : ''}

  shadow-color: #888;
  shadow-offset: 0 3px;
  shadow-opacity: 0.1;
  shadow-radius: 3px;
  elevation: 0.5;
`;

export const ButtonText = styled.Text<{ color: string; hasBorders: boolean }>`
  padding: ${(props) => (props.hasBorders ? '8px 15px' : '9px 16px')};
  color: ${(props) => props.color};
  text-align: center;
`;

export const LoadingIcon = styled.ActivityIndicator`
  top: -55px;
  width: 100%;
  height: 100%;
  align-self: center;
  background-color: rgba(125, 125, 125, 0.5);
`;
