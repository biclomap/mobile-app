import styled from 'styled-components/native';
import Layout from '../../../constants/Layout';

export const Screen = styled.View`
  padding: ${Layout.layout.gap}px;
`;

export const ScrollScreen = styled.ScrollView`
  padding: ${Layout.layout.gap}px;
`;
