import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import * as React from 'react';

const useCachedResources = () => {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    const loadResourcesAndDataAsync = async () => {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          medium: require('../assets/fonts/Roboto-Medium.ttf'),
          'medium-italic': require('../assets/fonts/Roboto-Italic.ttf'),
          regular: require('../assets/fonts/Roboto-Regular.ttf'),
          'regular-italic': require('../assets/fonts/Roboto-Italic.ttf'),
          light: require('../assets/fonts/Roboto-Light.ttf'),
          'light-italic': require('../assets/fonts/Roboto-LightItalic.ttf'),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    };

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
};

export default useCachedResources;
