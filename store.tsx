import { Action, ActionCreator, applyMiddleware, combineReducers, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import thunk, { ThunkAction } from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ThemeReducer from './constants/themeReduce';
import Authentication from './redux/Authentication';

const middleware = applyMiddleware(thunk);

const reducers = combineReducers({ ThemeReducer, authentication: Authentication });

export type IStore = ReturnType<typeof reducers>;

export type IThunkAction = ActionCreator<ThunkAction<Promise<Action>, IStore, void, any>>;

const persistConfig = {
  key: 'biclomap',
  storage: AsyncStorage,
  blacklist: ['ThemeReducer'],
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(persistedReducer, middleware);

export const persistor = persistStore(store);

export default store;
