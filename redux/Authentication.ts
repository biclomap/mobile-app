import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { logOutAsync } from 'expo-facebook';
import { Alert } from 'react-native';
import apiRequests from '../api/apiRequests';
import { IStore } from '../store';
import { getTypePrefixFromActionType, IStatus, prettyTextFromActionType, removeFromArray } from './general';
import HttpStatusCodes from './httpStatusCodes';

const name = 'authentication';

// ACTIONS
export const signUpAction = createAsyncThunk<{ email: string }, { email: string; password: string }>(`${name}/signUp`, async (data) => {
  const apiResponse = await apiRequests.POST.signUp(data);

  if (apiResponse.status === HttpStatusCodes.SUCCESS) {
    return { email: data.email };
  } else {
    throw new Error('Failed to sign up with email.');
  }
});

export const loginUserAction = createAsyncThunk<{ email: string; token: string }, { email: string; password: string }>(`${name}/login`, async (data) => {
  return { email: data.email, token: data.password };
});

export const logoutFacebookUserAction = createAsyncThunk<void, void>(`${name}/logoutFacebookUser`, async (_, { dispatch }) => {
  try {
    logOutAsync();
  } finally {
    dispatch(clearAuthState);
  }
});

// STATE MANAGEMENT

interface IState {
  id?: number;
  email?: string;
  name?: string;
  token?: string;
  status: IStatus;
  errors: { [key: string]: any };
}

const initialState: IState = {
  id: undefined,
  token: undefined,
  email: undefined,
  name: undefined,
  status: { pending: [], success: [], failed: [] },
  errors: {},
};

const authenticationSlice = createSlice({
  name: name,
  initialState: initialState,
  reducers: {
    cleanupRequestStatus(state, action: PayloadAction<string>) {
      state.status.pending = removeFromArray(state.status.pending, action.payload);
      state.status.success = removeFromArray(state.status.success, action.payload);
      state.status.failed = removeFromArray(state.status.failed, action.payload);
    },
    clearAuthState(state) {
      state = { ...initialState };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(signUpAction.fulfilled, (state, { payload }) => {
        state.email = payload.email;
      })
      .addCase(loginUserAction.fulfilled, (state, { payload }) => {
        state.email = payload.email;
        state.token = payload.token;
      })
      // GENERAL STATUS AND ERROR HANDLING \/
      .addMatcher(
        (action) => action.type.startsWith(`${name}/`) && action.type.endsWith('/pending'),
        (state, action) => {
          const cleanedError = { ...state.errors };
          delete cleanedError[action.type];
          state.errors = cleanedError;

          const typePrefix = getTypePrefixFromActionType(action.type);
          state.status.failed = removeFromArray(state.status.failed, typePrefix);
          state.status.success = removeFromArray(state.status.success, typePrefix);
          state.status.pending.push(typePrefix);
        },
      )
      .addMatcher(
        (action) => action.type.startsWith(`${name}/`) && action.type.endsWith('/fulfilled'),
        (state, action) => {
          const typePrefix = getTypePrefixFromActionType(action.type);
          state.status.pending = removeFromArray(state.status.pending, typePrefix);
          state.status.failed = removeFromArray(state.status.failed, typePrefix);
          state.status.success.push(typePrefix);
        },
      )
      .addMatcher(
        (action) => action.type.startsWith(`${name}/`) && action.type.endsWith('/rejected'),
        (state, action) => {
          const value = action.payload ? action.payload.error : action.error;
          state.errors = {
            ...state.errors,
            [action.type]: value,
          };

          const typePrefix = getTypePrefixFromActionType(action.type);
          state.status.pending = removeFromArray(state.status.pending, typePrefix);
          state.status.success = removeFromArray(state.status.success, typePrefix);
          state.status.failed.push(typePrefix);
          Alert.alert(prettyTextFromActionType(action.type), value.message);
        },
      );
  },
});

export const { cleanupRequestStatus, clearAuthState } = authenticationSlice.actions;

// SELECTORS

export const authSelector = (state: IStore) => state.authentication;
export const authRequestStatusSelector = (state: IStore) => state.authentication.status;

export default authenticationSlice.reducer;
