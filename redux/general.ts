export interface IStatus {
  pending: string[];
  success: string[];
  failed: string[];
}

export const getTypePrefixFromActionType = (type: string): string => type.replace(/(\/pending|\/fulfilled|\/rejected)$/, '');

export const removeFromArray = (array: string[], value: string): string[] => {
  const index = array.indexOf(value);
  if (index !== -1) {
    const newArray = [...array];
    newArray.splice(index, 1);
    return newArray;
  } else {
    return array;
  }
};

export const prettyTextFromActionType = (type: string): string => {
  const chopOff = type.endsWith('/rejected') ? '/rejected' : type.endsWith('/fulfilled') ? '/fulfilled' : type.endsWith('/pending') ? '/pending' : '';
  const shortened = type.slice(0, chopOff.length * -1).replace('/', ' ');
  return shortened.charAt(0).toUpperCase() + shortened.slice(1);
};
