import { IStore } from '../store';

const theme = (state: IStore) => state.ThemeReducer;

export default { theme };
