const colorPallet = {
  primary: {
    10: '#d0f0e6',
    20: '#a1e1cb',
    30: '#72d3b2',
    40: '#43c498',
    50: '#309975',
    60: '#267a5e',
    70: '#1d5c46',
    80: '#133d2f',
    90: '#0a1f17',
  },
  secondary: {
    10: '#d7dae4',
    20: '#aeb4c8',
    30: '#868fad',
    40: '#606c8e',
    50: '#454d66',
    60: '#373e52',
    70: '#292e3d',
    80: '#1c1f29',
    90: '#0e0f14',
  },
};

const colorSchemes = {
  light: {
    mainText: '#3e3f42',
    secondaryText: '#9a9a9a',
    background: '#f9f9f9',
    main: '#1665d8',
    success: '#34aa44',
    warning: '#f5ab2f',
    danger: '#e6492d',
    violet: '#6758f3',
    yellow: '#facf55',
    card: '#ffffff',
    tint: '#1665d8',
    icon: '#9ea0a5',
    grey50: '#bcbcbc',
    tabIconDefault: '#9eada5',
    tabIconSelected: '#1665d8',
    inputBorder: '#dadada',
    inputPlaceholder: '#a9a9a9',
    headerBackground: '#2a2a2f',
    headerText: '#e1e1e1',
    scheme: 'light',
    name: 'White',
  },
  dark: {
    mainText: '#e1e1e1',
    secondaryText: '#a1a1a1',
    background: '#232323',
    main: '#1665d8',
    success: '#34aa44',
    warning: '#f5ab2f',
    danger: '#e6492d',
    violet: '#6758f3',
    yellow: '#facf55',
    card: '#000000',
    tint: '#1665d8',
    icon: '#9ea0a5',
    grey50: '#bcbcbc',
    tabIconDefault: '#9eada5',
    tabIconSelected: '#1665d8',
    inputBorder: '#e9e9e9',
    inputPlaceholder: '#8a8a8a',
    headerBackground: '#000000',
    headerText: '#e1e1e1',
    scheme: 'dark',
    name: 'Dark',
  },
};

export default colorSchemes;

export type IColors = keyof typeof colorSchemes.light;

export type ITheme = { [key in IColors]: string };
