import { Appearance } from 'react-native';

import schemes from './Colors';
import { CHANGE_THEME } from './themeConstants';

const scheme = Appearance.getColorScheme() || 'light';

const initialState = schemes[scheme] || schemes.light;

const ThemeReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case CHANGE_THEME:
      const scheme = Object.keys(schemes).includes(action.data) ? (action.data as keyof typeof schemes) : 'light';
      return {
        ...schemes[scheme],
      };
    default:
      return state;
  }
};

export default ThemeReducer;
