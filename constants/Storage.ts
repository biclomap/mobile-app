import AsyncStorage from '@react-native-async-storage/async-storage';

const SetItem = async (key: string, value: string) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.log('Error while writing to local storage', error.message);
  }
};

const GetItem = async (key: string) => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value;
  } catch (error) {
    console.log('Error while reading from local storage', error.message);
  }
  return null;
};

export default { GetItem, SetItem };
