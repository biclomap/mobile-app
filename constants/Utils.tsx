const hexToRgb = (hex: string) => {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : {
        r: 0,
        g: 0,
        b: 0,
      };
};

export const calcUnderlayColor = (color: string) => {
  const rgb = hexToRgb(color);
  return `rgba(${rgb.r},${rgb.g},${rgb.b},0.1)`;
};
