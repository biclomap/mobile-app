import React from 'react';
import { Provider } from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { ActivityIndicator } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import Toast from 'react-native-toast-message';

import useCachedResources from './hooks/useCachedResources';
import StatusBar from './components/complex/statusBar/StatusBar';
import toastNotificationConfig from './components/complex/toast/toastNotificarionConfig';
import Navigation from './navigation';
import store, { persistor } from './store';

import { CenterPositioner } from './components/screens/home/styled';

const App = () => {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return (
      <CenterPositioner>
        <ActivityIndicator />
      </CenterPositioner>
    );
  } else {
    return (
      <Provider store={store}>
        <PersistGate loading={<ActivityIndicator />} persistor={persistor}>
          <SafeAreaProvider>
            <StatusBar />
            <Navigation />
            <Toast config={toastNotificationConfig} ref={(ref) => Toast.setRef(ref)} />
          </SafeAreaProvider>
        </PersistGate>
      </Provider>
    );
  }
};

export default App;
