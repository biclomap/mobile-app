export type RootStackParamList = {
  Auth: undefined;
  Root: undefined;
  NotFound: undefined;
};

export type AuthTabParamList = {
  Login: any;
  Landing: any;
  SignUp: any;
};

export type RootTabParamList = {
  Home: any;
  Map: any;
  Routes: any;
  Record: any;
  History: any;
  ComponentShowcase: any;
};
