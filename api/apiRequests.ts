import API, { IResponse } from './API';

const getRequests = {};

const postRequests = {
  signUp: (data: { email: string; password: string }): Promise<IResponse<{}>> => API.post('login/email/signup', data),
};

const apiRequests = {
  GET: getRequests,
  POST: postRequests,
};

export default apiRequests;
