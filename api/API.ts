import axios from 'axios';

const API = axios.create({
  baseURL: 'https://dev.biclomap.com/api/', // TODO: this should be pulled from the environment
  timeout: 15000,
});

API.interceptors.response.use(
  (response) => {
    return response;
  },
  function (error) {
    return Promise.resolve(error.response);
  },
);

export const addTokenToHeader = (token: string) => {
  API.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export const removeTokenFromHeader = () => {
  delete API.defaults.headers.common['Authorization'];
};

export interface IResponse<T> {
  config: {
    adapter: any;
    baseURL: string;
    data: object;
    headers: {
      Accept: string;
      'Content-Type': string;
    };
    maxBodyLength: number;
    maxContentLength: number;
    method: string;
    timeout: number;
    transformRequest: any[];
    transformResponse: any[];
    url: string;
    validateStatus: any[];
    xsrfCookieName: string;
    xsrfHeaderName: string;
  };
  data: T;
  headers: {
    'content-length': string;
    'content-type': string;
    date: string;
    via: string;
    'x-amz-apigw-id': string;
    'x-amz-cf-id': string;
    'x-amz-cf-pop': string;
    'x-amzn-requestid': string;
    'x-cache': string;
  };
  request: XMLHttpRequest;
  status: number;
  statusText: string | undefined;
}

export default API;
