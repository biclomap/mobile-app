import styled from 'styled-components/native';
import { ITheme } from '../constants/Colors';
import Layout from '../constants/Layout';

export const NavigationContainer = styled.View<{ theme: ITheme }>`
  background-color: ${(props) => `${props.theme.card}` || '#fff'};
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`;

export const NavigationButton = styled.TouchableHighlight<{ theme: ITheme }>`
  padding: ${Math.ceil(Layout.layout.gap / 3)}px;
`;

export const MiddleButton = styled.TouchableOpacity<{ theme: ITheme }>`
  margin-top: -20px;
  padding: ${Math.ceil(Layout.layout.gap / 3)}px;
  border-radius: ${Layout.layout.gap}px;
  background-color: ${(props) => `${props.theme.card}` || '#fff'};

  shadow-color: #888;
  shadow-offset: 0 10px;
  shadow-opacity: 0.8;
  shadow-radius: 10px;
  elevation: 2;
`;

export const SideButtons = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  width: ${Math.ceil(Layout.window.width / 2) - 20}px;
`;
