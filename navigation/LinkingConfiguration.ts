import * as Linking from 'expo-linking';
import { AuthTabParamList, RootTabParamList } from '../types';

const root: RootTabParamList = {
  Home: { screens: {} },
  Map: { screen: {} },
  Routes: { screen: {} },
  Record: { screen: {} },
  History: { screen: {} },
  ComponentShowcase: { screens: {} },
};

export const RootNavigationNames: { [key in keyof RootTabParamList]: keyof RootTabParamList } = {
  Home: 'Home',
  Map: 'Map',
  Routes: 'Routes',
  Record: 'Record',
  History: 'History',
  ComponentShowcase: 'ComponentShowcase',
};

const auth: AuthTabParamList = {
  Login: {
    screens: {},
  },
  Landing: {
    screens: {},
  },
  SignUp: {
    screens: {},
  },
};

export const AuthNavigationNames: { [key in keyof AuthTabParamList]: keyof AuthTabParamList } = {
  Login: 'Login',
  Landing: 'Landing',
  SignUp: 'SignUp',
};

type AppNavigationType = 'Auth' | 'Root' | 'NotFound';

export const AppNavigationNames: { [key in AppNavigationType]: AppNavigationType } = {
  Auth: 'Auth',
  Root: 'Root',
  NotFound: 'NotFound',
};

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Auth: {
        screens: auth,
      },
      Root: {
        screens: root,
      },
      NotFound: '*',
    },
  },
};
