import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import themeSelector from '../constants/themeSelector';

import { RootTabParamList } from '../types';
import { AppNavigationNames, RootNavigationNames } from './LinkingConfiguration';
import RootTabBar from './RootTabBar';
import HeaderBar from '../components/complex/headerBar/HeaderBar';

import Home from '../components/screens/home/Home';
import Map from '../components/screens/map/Map';
import ComponentShowcase from '../components/screens/componentShowcase/ComponentShowcase';
import PlaceholderScreen from '../components/screens/placeholderScreen/PlaceholderScreen';
import { authSelector } from '../redux/Authentication';

const StackNavigator = createStackNavigator<RootTabParamList>();

const HomeNavigator = () => (
  <StackNavigator.Navigator initialRouteName={RootNavigationNames.Home}>
    <StackNavigator.Screen
      name={RootNavigationNames.Home}
      component={Home}
      options={{ headerTitle: () => <HeaderBar title={'Biclomap'} />, headerLeft: () => null }}
    />
  </StackNavigator.Navigator>
);

const MapNavigator = () => (
  <StackNavigator.Navigator initialRouteName={RootNavigationNames.Map}>
    <StackNavigator.Screen
      name={RootNavigationNames.Map}
      component={Map}
      options={{ headerTitle: () => <HeaderBar title={'Map'} />, headerLeft: () => null }}
    />
  </StackNavigator.Navigator>
);

const RoutesNavigator = () => (
  <StackNavigator.Navigator initialRouteName={RootNavigationNames.Routes}>
    <StackNavigator.Screen
      name={RootNavigationNames.Routes}
      component={PlaceholderScreen}
      options={{ headerTitle: () => <HeaderBar title={'Routes'} />, headerLeft: () => null }}
    />
  </StackNavigator.Navigator>
);

const HistoryNavigator = () => (
  <StackNavigator.Navigator initialRouteName={RootNavigationNames.History}>
    <StackNavigator.Screen
      name={RootNavigationNames.History}
      component={PlaceholderScreen}
      options={{ headerTitle: () => <HeaderBar title={'History'} />, headerLeft: () => null }}
    />
  </StackNavigator.Navigator>
);

const ShowcaseNavigator = () => (
  <StackNavigator.Navigator initialRouteName={RootNavigationNames.ComponentShowcase}>
    <StackNavigator.Screen
      name={RootNavigationNames.ComponentShowcase}
      component={ComponentShowcase}
      options={{ headerTitle: () => <HeaderBar title={'Component Showcase'} />, headerLeft: () => null }}
    />
  </StackNavigator.Navigator>
);

const BottomTab = createBottomTabNavigator<RootTabParamList>();

const RootTabNavigator = () => {
  const theme = useSelector(themeSelector.theme);
  const user = useSelector(authSelector);

  const navigation = useNavigation();

  useEffect(() => {
    navigation.navigate(RootNavigationNames.Home);
  }, []);

  useEffect(() => {
    if (!user.token) {
      navigation.navigate(AppNavigationNames.Auth);
    }
  }, [user]);

  return (
    <BottomTab.Navigator tabBar={() => <RootTabBar theme={theme} />}>
      <BottomTab.Screen name={RootNavigationNames.Home} component={HomeNavigator} />
      <BottomTab.Screen name={RootNavigationNames.Map} component={MapNavigator} />
      <BottomTab.Screen name={RootNavigationNames.Routes} component={RoutesNavigator} />
      <BottomTab.Screen name={RootNavigationNames.History} component={HistoryNavigator} />
      <BottomTab.Screen name={RootNavigationNames.ComponentShowcase} component={ShowcaseNavigator} />
    </BottomTab.Navigator>
  );
};

export default RootTabNavigator;
