import React, { useState } from 'react';
import { Insets } from 'react-native';
import { getFocusedRouteNameFromRoute, useNavigation, useRoute } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome, faMapMarkedAlt, faRoute, faThList } from '@fortawesome/free-solid-svg-icons';
import { faDotCircle, faListAlt } from '@fortawesome/free-regular-svg-icons';
import { Shadow } from 'react-native-shadow-2';

import { ITheme } from '../constants/Colors';
import { RootNavigationNames } from './LinkingConfiguration';
import NewRideModal from '../components/complex/newRideModal/NewRideModal';
import { calcUnderlayColor } from '../constants/Utils';

import { MiddleButton, NavigationButton, NavigationContainer, SideButtons } from './styled';

interface IProps {
  theme: ITheme;
}

const RootTabBar = (props: IProps) => {
  const { theme } = props;

  const [touchingMiddleButton, setTouchingMiddleButton] = useState(false);
  const [showModal, setShowModal] = useState(false);

  const navigation = useNavigation();
  const path = getFocusedRouteNameFromRoute(useRoute());

  const underlayColor = calcUnderlayColor(theme.main);

  const middleButtonHitSlop: Insets = {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  };

  const color = (name: keyof typeof RootNavigationNames) => (path === name ? theme.main : theme.icon);

  return (
    <>
      <Shadow startColor={'rgba(0,0,0,0.1)'} distance={3}>
        <NavigationContainer theme={theme}>
          <SideButtons>
            <NavigationButton
              onPress={() => navigation.navigate(RootNavigationNames.Home)}
              underlayColor={underlayColor}
            >
              <FontAwesomeIcon icon={faHome} size={24} color={color(RootNavigationNames.Home)} />
            </NavigationButton>
            <NavigationButton
              onPress={() => navigation.navigate(RootNavigationNames.Map)}
              underlayColor={underlayColor}
            >
              <FontAwesomeIcon icon={faMapMarkedAlt} size={24} color={color(RootNavigationNames.Map)} />
            </NavigationButton>
            <NavigationButton
              onPress={() => navigation.navigate(RootNavigationNames.Routes)}
              underlayColor={underlayColor}
            >
              <FontAwesomeIcon icon={faRoute} size={24} color={color(RootNavigationNames.Routes)} />
            </NavigationButton>
          </SideButtons>
          <MiddleButton
            theme={theme}
            onPress={() => setShowModal(true)}
            activeOpacity={1}
            onPressIn={() => setTouchingMiddleButton(true)}
            onPressOut={() => setTouchingMiddleButton(false)}
            hitSlop={middleButtonHitSlop}
          >
            <FontAwesomeIcon icon={faDotCircle} size={40} color={touchingMiddleButton ? theme.main : theme.icon} />
          </MiddleButton>
          <SideButtons>
            <NavigationButton
              onPress={() => navigation.navigate(RootNavigationNames.History)}
              underlayColor={underlayColor}
            >
              <FontAwesomeIcon icon={faListAlt} size={24} color={color(RootNavigationNames.History)} />
            </NavigationButton>
            <NavigationButton
              onPress={() => navigation.navigate(RootNavigationNames.ComponentShowcase)}
              underlayColor={underlayColor}
            >
              <FontAwesomeIcon icon={faThList} size={24} color={color(RootNavigationNames.ComponentShowcase)} />
            </NavigationButton>
          </SideButtons>
        </NavigationContainer>
      </Shadow>
      <NewRideModal visible={showModal} onClose={() => setShowModal(false)} />
    </>
  );
};

export default RootTabBar;
