import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { RootStackParamList } from '../types';
import AuthTabNavigator from './AuthTabNavigator';
import RootTabNavigator from './RootTabNavigator';
import LinkingConfiguration, { AppNavigationNames } from './LinkingConfiguration';
import NotFoundScreen from '../components/screens/NotFoundScreen';

import themeSelector from '../constants/themeSelector';
import { authSelector } from '../redux/Authentication';

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
const Navigation = () => {
  const theme = useSelector(themeSelector.theme);

  const navigationTheme = {
    dark: theme.scheme === 'dark',
    colors: {
      primary: theme.main,
      background: theme.background,
      card: theme.background,
      text: theme.mainText,
      border: theme.secondaryText,
      notification: theme.mainText,
    },
  };

  return (
    <NavigationContainer linking={LinkingConfiguration} theme={navigationTheme}>
      <MainNavigator />
    </NavigationContainer>
  );
};

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

const MainNavigator = () => {
  const theme = useSelector(themeSelector.theme);
  const user = useSelector(authSelector);

  const initialRoute = user.token ? AppNavigationNames.Root : AppNavigationNames.Auth;

  return (
    <Stack.Navigator
      initialRouteName={initialRoute}
      screenOptions={{
        headerShown: false,
        animationEnabled: theme.scheme === 'light', //There is a white background artifact on changing navigators
      }}
    >
      <Stack.Screen name={AppNavigationNames.Auth} component={AuthTabNavigator} />
      <Stack.Screen name={AppNavigationNames.Root} component={RootTabNavigator} />
      <Stack.Screen name={AppNavigationNames.NotFound} component={NotFoundScreen} options={{ title: 'Oops!' }} />
    </Stack.Navigator>
  );
};

export default Navigation;
