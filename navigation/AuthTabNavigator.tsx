import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../components/screens/login/Login';
import AuthLanding from '../components/screens/authLanding/AuthLanding';

import { AuthTabParamList } from '../types';
import { AuthNavigationNames } from './LinkingConfiguration';
import HeaderBar from '../components/complex/headerBar/HeaderBar';
import SignUp from '../components/screens/signUp/SignUp';

const AuthStackNavigator = createStackNavigator<AuthTabParamList>();

const AuthNavigator = () => (
  <AuthStackNavigator.Navigator initialRouteName={AuthNavigationNames.Landing}>
    <AuthStackNavigator.Screen
      name={AuthNavigationNames.Landing}
      component={AuthLanding}
      options={{ headerTitle: () => <HeaderBar title={'Biclomap'} />, headerLeft: () => null }}
    />
    <AuthStackNavigator.Screen
      name={AuthNavigationNames.Login}
      component={Login}
      options={{
        headerTitle: () => <HeaderBar title={'Login'} back={AuthNavigationNames.Landing} />,
        headerLeft: () => null,
      }}
    />
    <AuthStackNavigator.Screen
      name={AuthNavigationNames.SignUp}
      component={SignUp}
      options={{
        headerTitle: () => <HeaderBar title={'Singing Up'} back={AuthNavigationNames.Landing} />,
        headerLeft: () => null,
      }}
    />
  </AuthStackNavigator.Navigator>
);

export default AuthNavigator;
