import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LinkingConfiguration from '../LinkingConfiguration';

const Stack = createStackNavigator();
const MockedNavigator = ({ component, params = {} }: { component: any; params?: object }) => {
  return (
    <NavigationContainer linking={LinkingConfiguration}>
      <Stack.Navigator>
        <Stack.Screen name='MockedScreen' component={component} initialParams={params} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MockedNavigator;
